//for the color box jquery
			jQuery(document).ready(function(){
				//Examples of how to assign the ColorBox event to elements				
				$(".group2").colorbox({rel:'group2', transition:"fade"});	
                                $(".group21").colorbox({rel:'group21', transition:"fade"});
				$(".ajax").colorbox();
				$(".iframe").colorbox({iframe:true, width:"950px", height:"450px"});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});