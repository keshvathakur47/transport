<?php
/*
 * Date Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web Management system
 *   
 */

class date{
    
   function ToUKDate($date)
    {
            return date("d-m-Y", strtotime($date));
    }
   
    function ToUSDate($date)
    {

    $date= date("m/d/Y",strtotime($date));
    $Parts=array();
    $Parts=explode('/',$date);
    $Result=$Parts['2'].'-'.$Parts['0'].'-'.$Parts['1'];
    return $Result;
    }
    
    
    function add_days_to_date($days,$date)
    {

            if($days==1):
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +1 day");
            else:
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +".$days." days");
            endif;
            $date_result=date('y-m-d', $date );

            return $date_result;	
    }

    function subtract_days_from_date($days,$date)
    {

            if($days==1):
                $date = strtotime(date("Y-m-d", strtotime($date)) . " -1 day");
            else:
                $date = strtotime(date("Y-m-d", strtotime($date)) . " -".$days." days");
            endif;
            $date_result=date('y-m-d', $date );

            return $date_result;	
    }


    function dateDiff($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            if($end_ts>$start_ts):

                 $diff = $end_ts - $start_ts;

                 return round($diff / 86400);

            else:

                return 0;

            endif;


    }
    
    
}

$date_obj= new date();

?>