<?php

/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's Management System
 */

class paymentHistory extends cwebc {

    function __construct() {
        parent::__construct('payment_history');
        $this->requiredVars = array('id', 'website_subscription', 'amount', 'date_of_payment', 'payment_method', 'transaction_id', 'on_date', 'plan_id', 'subscription_title', 'website_id');
    }

    public static function isSavedHistory($website_sub_id, $website_id) {
        $obj = new paymentHistory;
        $obj->Field = 'id';
        $obj->Where = " WHERE website_subscription='$website_sub_id' AND website_id='$website_id'";
        $data = $obj->DisplayOne();
        return is_object($data) ? TRUE : FALSE;
    }

    # Save Payment History

    function saveHistory($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    # List Subscription Payment History

    function listSubscriptionPayments($sub_id) {
        $this->Where = "where website_subscription='" . $sub_id . "' ORDER BY on_date desc";

        return $this->ListOfAllRecords('object');
    }

    # List Subscription Payment History by website id

    function listWebsitePayments($site_id) {
        $this->Where = "where website_id='" . $site_id . "' ORDER BY on_date desc";

        return $this->ListOfAllRecords('object');
    }

    # Get Total Income

    function getTotalIncome() {
        $this->Field = 'sum(amount) as total';
        $this->Where = "where id!=''";
        $rec = $this->DisplayOne();
        if ($rec):
            return $rec->total;
        else:
            return '0';
        endif;
    }

    # get Monthly Payment History

    function getMonthlyPaymentHistory($year) {
        $rec = array();
        $this->Field = 'id,website_id, SUBSTRING(MONTHNAME(date_of_payment), 1, 3) AS month_y, sum(amount) as total';
        $this->Where = "where YEAR(date_of_payment)='" . $year . "' GROUP BY MONTHNAME(date_of_payment) ORDER BY date_of_payment desc";

        $record = $this->ListOfAllRecords('object');

        if (!empty($record)):
            foreach ($record as $key => $value):
                $rec[$value->month_y] = $value->total;
            endforeach;
        endif;

        $month = array('Jan' => 0, 'Feb' => 0, 'Mar' => 0, 'Apr' => 0, 'May' => 0, 'Jun' => 0, 'Jul' => 0, 'Aug' => 0, 'Sep' => 0, 'Oct' => 0, 'Nov' => 0, 'Dec' => 0);

        $mth_inc = '';
        foreach ($month as $mk => $mv):
            if (array_key_exists($mk, $rec)):
                $mth_inc.="['" . $mk . "'," . $rec[$mk] . "],";
            else:
                $mth_inc.="['" . $mk . "'," . $mv . "],";
            endif;
        endforeach;

        $mth_inc = substr_replace(trim($mth_inc), "", -1);

        return $mth_inc;
    }

}

?>