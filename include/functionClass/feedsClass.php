<?php

/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 */

class feeds extends cwebc {

    function __construct() {
        parent::__construct('feeds');
        $this->requiredVars = array('id', 'name', 'url', 'folder_name', 'is_active', 'is_deleted', 'date_add', 'date_upd');
    }

    function getAllActiveFeeds() {
        $this->Where = " where is_active='1' and is_deleted='0' order by name asc";
        return $this->ListOfAllRecords('object');
    }

    public static function getFeedIdByFolder($folder) {
        $obj = new feeds();
        $obj->Field = "id";
        $obj->Where = "where is_active='1' and folder_name='$folder'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : FALSE;
    }

    public static function checkFeedActive($id) {
        $query = new query('feeds');
        $query->Where = " where id= '$id' is_active='1' and is_deleted='0'";
        $object = $query->DisplayOne();
        if (is_object($object)) {
            return true;
        }
        return false;
    }

}

class user_website_feed extends cwebc {

    function __construct() {
        parent::__construct('user_website_feed');
        $this->requiredVars = array('id', 'user_website', 'feed_id');
    }

    function resetWebsiteFeeds($website_id) {
        $this->Where = " where user_website='$website_id'";
        return $this->Delete_where();
    }

    function addWebsiteFeedRel($website_id, $feed_id) {
        $this->Data['user_website'] = $website_id;
        $this->Data['feed_id'] = $feed_id;
        return $this->Insert();
    }

    function getWebsiteFeeds($website_id) {
        $this->Field = " feed_id";
        $this->Where = " where user_website='$website_id'";
        return $this->ListOfAllRecords('object');
    }

    function getAllWebsitesIdByFeedId($feed_id) {
        $this->Where = " where feed_id='$feed_id'";
        return $this->ListOfAllRecords('object');
    }

    public static function getFeedSubscribeDate($website_id) {
        $obj = new user_website_feed;
        $obj->Where = "where user_website='$website_id'";
        $data = $obj->DisplayOne();
        return is_object($data)?$data->date_add:FALSE;
    }

    public static function getFeedIdByWebsiteID($website_id) {
        $obj = new user_website_feed;
        $obj->Where = "where user_website='$website_id'";
        $data = $obj->DisplayOne();
        return $data->feed_id;
    }

}

?>