<?php

/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's Management System
 */

class user extends cwebc {

    function __construct() {
        parent::__construct('user');
        $this->requiredVars = array('id', 'first_name', 'last_name', 'username', 'password', 'phone', 'email', 'address1', 'address2', 'city', 'state', 'country', 'zip', 'signup_date', 'last_login_date', 'company_name', 'company_detail', 'is_email_verified', 'is_active', 'is_deleted', 'ip_address', 'verify_code', 'reset_password', 'delete_code', 'deactivate', 'is_website_subscribe');
    }

    #Get List of all Active Users

    function getActiveUsers($result_type = 'object') {

        $this->Where = "where is_deleted='0' AND is_active='1'";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    #Get List of all Users

    function getAllUsers($result_type = 'object') {

        $this->Where = "where is_deleted='0' ";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    #Get List of all Deleted Users

    function listDeletedUsers($result_type = 'object') {
        $this->Where = "where is_deleted='1'";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    #Create/Edit new User

    function saveUserDetail($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($POST['password'])):
            $this->Data['password'] = md5($this->Data['password']);
        endif;
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $this->Data['signup_date'] = date('Y:m:d H:m:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    #get User

    function getUser($id) {
        return $this->_getObject('user', $id);
    }

    //get UserName By Id
    public static function getUsernameById($id) {
        $obj = new user();
        $obj->Where = "where id='$id'";
        $data = $obj->DisplayOne();
        return $data->username;
    }

    #Check Single Field exist Or Not

    function check_field_value_exist($field, $value, $id = 0) {

        if ($id && $id != 0 && $id != ""):
            $this->Where = "where `" . $field . "`='" . mysql_real_escape_string($value) . "' AND id!='" . $id . "'";
        else:
            $this->Where = "where `" . $field . "`='" . mysql_real_escape_string($value) . "'";
        endif;

        $user = $this->DisplayOne();
        if ($user && is_object($user)):
            return $user;
        else:
            return false;
        endif;
    }

    /* set user email verified */

    function setEmailVerified($id) {
        $this->Data['id'] = $id;
        $this->Data['is_email_verified'] = '1';
        $this->Data['verify_code'] = '';
        $this->Update();
        return true;
    }

    /* set user reset key */

    function saveResetKey($id, $key, $value) {
        $this->Data['id'] = $id;
        $this->Data[$key] = $value;
        $this->Update();
        return true;
    }

    /* set deactivate account */

    function saveDeactivate($id) {
        $this->Data['id'] = $id;
        $this->Data['deactivate'] = '1';
        $this->Data['delete_code'] = '';
        $this->Update();
        return true;
    }

    /*
     * set account deactivate after verification
     */

    function setDeactivate($id) {
        $this->Data['id'] = $id;
        $this->Data['is_active'] = '0';
        $this->Data['deactivate'] = '1';
        $this->Data['delete_code'] = '';
        $this->Update();
        return true;
    }

    /* set user reset password */

    function saveResetPassword($id, $password) {
        $this->Data['id'] = $id;
        $this->Data['password'] = md5($password);
        $this->Data['reset_password'] = '';
        $this->Data['is_email_verified'] = '1';
        $this->Update();
        return true;
    }

    /*
     * get user from email token
     */

    function getUserByEmailToken($token) {
        $this->Where = " where is_deleted='0' and verify_code='" . $token . "'";
        $user = $this->DisplayOne();
        if (is_object($user)) {
            return $user;
        } else {
            return false;
        }
    }

    #delete a page by id     

    function deleteUser($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    #Get count of all users

    function countUsers($show_active = 0) {
        $total_count = 0;
        $this->Field = "id";
        if ($show_active):
            $this->Where = "where is_deleted='0' AND is_active='1' ";
        else:
            $this->Where = "where is_deleted='0' ";
        endif;

        $object = $this->ListOfAllRecords('object');
        if (count($object)):
            $total_count = count($object);
        endif;
        return $total_count;
    }

    function setUserWebsiteSubscribed($id) {
        $this->Data['id'] = $id;
        $this->Data['is_website_subscribe'] = '1';
        return $this->Update();
    }

    public static function checkEmail($email) {
        $query = new user;
        $query->Field = "COUNT(*) as count";
        $query->Where = "WHERE `email` = '$email'";
        $data = $query->DisplayOne();
        return $data->count;
    }
    public static function checkEmailPassword($email, $password) {
        $query = new user;
        $query->Where = "WHERE `email` = '$email' AND `password` = '$password'";
        return $query->DisplayOne();
    }
}
?>