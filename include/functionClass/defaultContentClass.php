<?php
/*
 * Default Content Pages Module Class - 
 * You are not adviced to make edits into this class.
 *   
 */

class defaultContent extends cwebc {
    
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($order='asc', $orderby='id', $parent_id=0){
        parent::__construct('default_content');
            $this->orderby=$orderby;
            $this->parent_id=$parent_id;
            $this->order=$order;
            $this->requiredVars=array('id', 'name', 'urlname','is_default','is_deleted', 'page', 'short_description','page_name', 'meta_keyword', 'meta_description', 'date_add','date_upd');
    }

    /*
     * Create new page or update existing page
     */
    function savePage($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        
        if($this->Data['page_name']==''){
            $this->Data['page_name']=$this->Data['name'];
        }
        
        if($this->Data['meta_keyword']==''){
            $this->Data['meta_keyword']=$this->Data['name'];
        }
        
        if($this->Data['meta_description']==''){
            $this->Data['meta_description']=$this->Data['name'];
        }
        
        if($this->Data['urlname']==''){
            $this->Data['urlname']=$this->_sanitize($this->Data['name']);
        }
        
        $this->Data['is_default']=isset($this->Data['is_default'])?'1':'0';
        $this->Data['is_deleted']=isset($this->Data['is_deleted'])?'1':'0';

        if(isset($this->Data['id']) && $this->Data['id']!=''){
       
            if($this->Update())
              return $this->Data['id'];
        }
        else{
          
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get page by id
     */
    function getPage($id){
        $this->Where=" where `id` = $id";
        return $this->DisplayOne();
    }
    
    
    /*
     * Get List of all pages in array
     */
    function listPages(){
        $this->Where="where is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();        
    }
    
    /*
     * get all default pages
     */
    function getAllDefaultPages(){
        $this->Where="where is_deleted='0' and is_default='1' order by id asc";
        return $this->ListOfAllRecords('object');        
    }
    
    /*
     * delete a page by id
     */
    function deletePage($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }
    
    function getPageContent($id){
        return html_entity_decode($this->getPage($id)->page);
    }
    
    function getPageTitle($id){
        return $this->getPage($id)->page_name;
    }
    
    function getPageSlug($id){
        return $this->getPage($id)->urlname;
    }
}
?>