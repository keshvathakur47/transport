<?php

/*
 * Abstract Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class cwebc extends query {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($table) {
        parent::__construct($table);
        $this->TableName = TABLE_PREFIX . $table;
        $this->orderby = 'position';
        $this->order = 'asc';
    }

    /*
     * Create new page or update existing page
     */

    function saveObject($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? 1 : 0;
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Create new page or update existing page with seo information
     */

    function saveObjectWithSeo($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? 1 : 0;

        if ($this->Data['meta_name'] == '') {
            $this->Data['meta_name'] = $this->Data['name'];
        }

        if ($this->Data['meta_keyword'] == '') {
            $this->Data['meta_keyword'] = $this->Data['name'];
        }

        if ($this->Data['meta_description'] == '') {
            $this->Data['meta_description'] = $this->Data['name'];
        }

        if ($this->Data['urlname'] == '') {
            $this->Data['urlname'] = $this->_sanitize($this->Data['name']);
        }

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get page by id
     */

    function getObject($id) {
        return $this->_getObject($this->TableName, $id);
    }

    /*
     * Get List of all pages in array
     */

    function ListAll() {
        $this->Where = "where is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();
    }

    /*
     * delete a page by id
     */

    function deleteObject($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /*
     * permanently delete an item by id
     */

    function purgeObject($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Restore Item an item by id
     */

    function restoreObject($id) {
        $this->id = $id;
        return $this->Restore();
    }

    /*
     * Fetch all deleted events - which have "is_deleted" set to "1"
     */

    function getThrash() {
        $this->Where = "where is_deleted='1'";
        $this->DisplayAll();
    }

    /*
     * Update page position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    function getVar($var, $id) {
        return html_entity_decode($this->getObject($id)->$var);
    }

    /*
     * Enable Paging
     */

    function enablePaging($pageNo = 1, $pageSize = 10) {
        $this->AllowPaging = 1;
        $this->PageNo = $pageNo;
        $this->PageSize = $pageSize;
    }

    function getTotalPages() {
        return $this->TotalPages;
    }

    function getTotalRecords() {
        return $this->TotalRecords;
    }

    public static function concatenateIdWithSlug($table, $id) {
        $query = new query($table);
        $query->Where = " urlname=concat(urlname,'-', id) ";
        $query->Where.=" where id=$id";
        return $query->UpdateCustom();
    }

}

?>