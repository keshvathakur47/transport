<?php
/*
 * Shipping Weight Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class shipping_weight extends shipping {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($table = 'supplier_shipping_weight',$order='desc', $orderby='id'){
        parent::__construct($table);
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id', 'supplier_id', 'shipping_id', 'zone_id', 'is_free','price', 'tax_rule_id','is_active','is_deleted','shipping_group','min','max');
    }
    /*
     * list price shippings
     */
    function listWeightShipping($active=false){
        global $supplier_id;
        $query = new query('supplier_shipping_weight as sw');
        $query -> Field=" zn.name as zone,sg.name as group_name,sw.* ";
        $query->Where = " left join ".TABLE_PREFIX."supplier_zone as zn on sw.zone_id=zn.id";
        $query->Where .= " left join ".TABLE_PREFIX."supplier_shipping_group as sg on sw.shipping_group=sg.id";
        $query->Where .= " where sw.supplier_id = '$supplier_id' ";
        if($active):
        $query->Where.= " and sw.is_active='1'";
        endif;
        $query->Where.= " and sw.is_deleted='0' order by sw.id desc";
        return $query->ListOfAllRecords('object');

    }
    /*
     * insert or update shipping rule
     */
    function saveShipping($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        global $supplier_id;
        $this->Data['shipping_id']=SHIPPING_WEIGHT_ID;
        $this->Data['supplier_id']=$supplier_id;
        $this->Data['is_active']=isset($POST['is_active'])?"1":"0";
        
        $this->Data['max']=($this->Data['max']=='')?"-1":$this->Data['max'];
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $id = $this->Data['id'];
            if($this->Update())
              return $id;
        }
        else{
            if($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }
    
    /*
     * get object of shiping weight entry
     */
    function getShippingWeight($id){
        global $supplier_id;
        $this->Where=" where id='$id' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }
    /*
     * calculate shipping by zone id
     
    function calculateShippingByZone($zone_id){
        $Query = new shipping_weight();
        $Query->Where=" where zone_id='$zone_id' and is_active='1' and is_deleted='0'";
        $object = $Query->DisplayOne();
        $return = array();
        if($object):
            $return['shipping_method_name'] = 'Weight Based Shipping';
            $return['shipping_rule_id'] = $object->id;
            $return['is_shipping_free']='0';
            $return['shipping_method_id']=SHIPPING_WEIGHT_ID;
            
            $weight = cart_detail::getCartWeight();

            $queryObj = new shipping_weight_range();
            $weight_range = $queryObj->getRangeByWeight($object->id,$weight);
            if($weight_range):
                $return['total_shipping_amount'] = number_format($weight_range->price,'2');
            else:
                $return['total_shipping_amount']='0';
            endif;
            return $return;
        endif;
        return false;
    }
    */
    /*
     * calculate shipping by zone id
     */
    function calculateShippingByZone($zone_id){
        
        $weight = cart_detail::getCartWeight();
        
        $Query = new query('supplier_shipping_weight as sw, supplier_shipping_group as sg');
        $Query->Field = "sg.name,sg.delivery_days,sw.*";
        $Query->Where =" where (sw.zone_id='$zone_id' or sw.zone_id='0') and sw.min <= $weight and (sw.max >= $weight or sw.max =  '-1') and sw.is_deleted='0'";        
        $Query->Where .=" and sw.shipping_group = sg.id";
        $Query->Where .= createUserGroupCondition('logged_in_user','sg');
        $Query->Where .=" group by sw.shipping_group order by sg.position asc";
        $applied_price_rules = $Query->ListOfAllRecords('object');

        $return = array();
        
        if(count($applied_price_rules)):
            foreach($applied_price_rules as $kk=>$vv):
                
                $expected_delivery_date_min  = false;
                if(is_numeric($vv->min_delivery_days) && $vv->min_delivery_days > 0): 
                    $expected_delivery_date_min = date::add_weekdays_to_date($vv->min_delivery_days, date("Y-m-d"),true);
                endif;
                
                $expected_delivery_date_max  = false;
                if(is_numeric($vv->max_delivery_days) && $vv->max_delivery_days > 0):
                    $expected_delivery_date_max = date::add_weekdays_to_date($vv->max_delivery_days, date("Y-m-d"),true);
                endif;
                
                $rule_array = array(
                    "shipping_method_name" => $vv->name,
                    "shipping_rule_id" => $vv->id,
                    "is_shipping_free" => '0',
                    "shipping_method_id" => $vv->shipping_id,
                    "total_shipping_amount" => number_format($vv->price,'2'),
                    "shipping_group_id" => $vv->shipping_group,
                    "estimate_delivery_date_min" => $expected_delivery_date_min,
                    "estimate_delivery_date_max" => $expected_delivery_date_max
                );
                
                $return[] = $rule_array;
            endforeach;
        endif;

        return $return;
    }
}

class shipping_weight_range extends shipping_weight {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($table = 'supplier_shipping_weight_range',$order='desc', $orderby='id'){
        parent::__construct($table);
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id', 'supplier_id', 'weight_rule_id', 'min', 'max','price','is_deleted');
    }
    
    /*
     * insert or update shipping rule
     */
    function saveShippingWeightRange($POST){
        global $supplier_id;
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['supplier_id']=$supplier_id;
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $id = $this->Data['id'];
            if($this->Update())
              return $id;
        }
        else{
            if($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }
    
    /*
     * list all shipping weight ranges of rule
     */
    function listShippingWeightRange($weight_rule_id){
        global $supplier_id;
        $this->Where=" where supplier_id='$supplier_id' and weight_rule_id='$weight_rule_id' and is_deleted='0' order by min asc";
        return $this->ListOfAllRecords();
    }
    
    /*
     * get last max value 
     */
    function getLastMaxWeightValue($weight_rule_id){
        global $supplier_id;
        $this->Where=" where supplier_id='$supplier_id' and weight_rule_id='$weight_rule_id' and is_deleted='0' order by min desc limit 0,1";
        $object = $this->DisplayOne();
        if(is_object($object)):
            return $object->max;
        endif;
        return false;
    }
    /*
     * get price range from price
     */
    function getRangeByWeight($rule_id,$weight){
        global $supplier_id;
        $this->Where=" where supplier_id='$supplier_id' and weight_rule_id=$rule_id and `min` < $weight and (`max` > $weight or `max` =  '-1') and is_deleted='0'";
        //$this->print=1;
        return $this->DisplayOne();
    }
}