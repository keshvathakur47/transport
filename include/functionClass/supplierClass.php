<?php

/*
 * Supplier Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's Management System
 */

class supplier extends cwebc {

    function __construct() {
        parent::__construct('supplier');
        $this->requiredVars = array(
            'id',
            'username',
            'password',
            'email',
            'name',
            'folder_name',
            'is_active',
            'is_deleted',
            'date_add',
            'date_upd',
        );
    }

    #Get List of all Active Suppliers

    function getActiveSuppliers($return = FALSE) {
        $this->Where = "where is_deleted='0' AND is_active='1'";
        return $return === TRUE ? $this->ListOfAllRecords() : $this->DisplayAll();
    }

    #Get List of all non-deleted Suppliers

    function getAllSuppliers($return = FALSE) {
        $this->Where = "WHERE is_deleted='0' ";
        return $return === TRUE ? $this->ListOfAllRecords() : $this->DisplayAll();
    }

    #Get List of all Deleted Suppliers

    function getDeletedSuppliers() {
        $this->Where = "where is_deleted='1'";
        return $this->DisplayAll();
    }

    #Create/Edit new Supplier

    function saveSupplier($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($POST['password'])) {
            $this->Data['password'] = md5($this->Data['password']);
        }
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
            return FALSE;
        } else {
            $this->Data['date_add'] = date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    #get Supplier

    function getSupplier($id) {
        return $this->_getObject('supplier', $id);
    }

    public static function AlreadyExists($value, $Field = 'username') {
        $obj = new supplier;
        $obj->Field = 'id';
        $obj->Where = " WHERE $Field='$value'";
        $data = $obj->DisplayOne();
        return is_object($data) ? TRUE : FALSE;
    }

    public static function AnotherExists($id, $value, $Field = 'username') {
        $obj = new supplier;
        $obj->Field = 'id';
        $obj->Where = " WHERE $Field='$value' AND id != '$id'";
        $data = $obj->DisplayOne();
        return is_object($data) ? TRUE : FALSE;
    }

    /*
     * Check if a Supplier is present in table and deleted flag is not set
     */

    public static function isValidSupplier($id) {
        $obj = new supplier;
        $obj->Field = 'id';
        $obj->Where = " WHERE id='$id' AND is_deleted='0'";
        $data = $obj->DisplayOne();
        return is_object($data) ? TRUE : FALSE;
    }

    /*
     * Check if a Supplier is present in table
     */

    public static function isSupplier($id) {
        $obj = new supplier;
        $obj->Field = 'id';
        $obj->Where = " WHERE id='$id'";
        $data = $obj->DisplayOne();
        return is_object($data) ? TRUE : FALSE;
    }

    public static function SoftDeleteSupplier($id) {
        $obj = new supplier;
        $obj->id = $id;
        return $obj->SoftDelete();
    }

    public static function RestoreSupplier($id) {
        $obj = new supplier;
        $obj->id = $id;
        return $obj->Restore();
    }

    public static function HardDeleteSupplier($id) {
        $obj = new supplier;
        $obj->id = $id;
        return $obj->Delete();
    }

    public static function isValidPassword($supplier_id, $password, $encrypt = TRUE) {
        if ($encrypt === TRUE) {
            $password = md5($password);
        }
        $obj = new supplier;
        $obj->Where = " WHERE id='$supplier_id' AND password='$password'";
        $data = $obj->DisplayOne();
        return is_object($data) ? TRUE : FALSE;
    }

    /* set supplier reset key */

    function saveResetKey($id, $key, $value) {
        $this->Data['id'] = $id;
        $this->Data[$key] = $value;
        $this->Update();
        return true;
    }

    #Check Single Field exist Or Not

    function check_field_value_exist($field, $value, $id = 0) {
        if ($id && $id != 0 && $id != "") {
            $this->Where = " WHERE `$field`='" . mysql_real_escape_string($value) . "' AND id!='$id'";
        } else {
            $this->Where = " WHERE `$field`='" . mysql_real_escape_string($value) . "'";
        }
        $data = $this->DisplayOne();
        return is_object($data) ? $data : FALSE;
    }

    function getSupplierByFolderName($folder) {
        $this->Where = " WHERE folder_name='$folder'";
        return $this->DisplayOne();
    }

    public static function checkUsernamePassword($username, $password) {
        $query = new supplier;
        $query->Where = "WHERE `username` = '$username' AND `password` = '$password'";
        return $query->DisplayOne();
    }

    public static function checkEmail($email) {
        $query = new supplier;
        $query->Where = "WHERE `email` = '$email'";
        return $query->DisplayOne();
    }

}

class supplier_product_details extends cwebc {

    function __construct() {
        parent::__construct('supplier_product_details');
        $this->requiredVars = array(
            'id',
            'supplier_id',
            'sku',
            'name',
            'wholesale_price',
            'unit_price',
            'date_add',
            'date_upd',
        );
    }

    function saveProductDetails($Data) {
        global $supplier_id;
        $this->Data = $this->_makeData($Data, $this->requiredVars);
        if (isset($Data['id']) && $Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        } else {
            $this->Data['date_add'] = date('Y-m-d H:i:s');
            $this->Data['supplier_id'] = $supplier_id;
//            $this->print = 1;
            $this->Insert();
            return $this->GetMaxId();
        }
        return FALSE;
    }

    public static function isAlreadySaved($supplier_id, $sku) {
        $obj = new supplier_product_details;
        $obj->Field = 'id';
        $obj->Where = " WHERE supplier_id='$supplier_id' AND sku='$sku'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : FALSE;
    }

    public static function AlreadySavedDetails($supplier_id, $sku) {
        $obj = new supplier_product_details;
        $obj->Field = '*';
        $obj->Where = " WHERE supplier_id='$supplier_id' AND sku='$sku'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : FALSE;
    }

    function deleteProductDetails($supplier_id, $sku) {
        $this->Where = " WHERE supplier_id='$supplier_id' AND sku='$sku'";
        $this->Delete_where();
        $obj = new supplier_product_details;
        $Query = "ALTER TABLE `$obj->TableName` AUTO_INCREMENT =1;";
        $obj->ExecuteQuery($Query);
        return TRUE;
    }

    function getAllProductModifications() {
        global $supplier_id;
        $this->Where = " WHERE supplier_id=$supplier_id";
        $data = $this->ListOfAllRecords();
        $mods = [];
        if (!empty($data)) {
            foreach ($data as $mod) {
                $mods[$mod['sku']] = array(
                    'name' => $mod['name'],
                    'wholesale_price' => $mod['wholesale_price'],
                    'unit_price' => $mod['unit_price'],
                );
            }
        }
        return $mods;
    }

}

class supplier_allowed_manufacturer extends cwebc {

    function __construct() {
        parent::__construct('supplier_allowed_manufacturer');
        $this->requiredVars = array(
            'id',
            'supplier_id',
            'manufacturer'
        );
    }

    public static function isAllowedManufacturer($manufacturer) {
        global $supplier_id;
        $obj = new supplier_allowed_manufacturer;
        $obj->Where = " WHERE manufacturer='$manufacturer' AND supplier_id=$supplier_id";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : FALSE;
    }

    public static function getAllAllowedManufacturer() {
        global $supplier_id;
        $obj = new supplier_allowed_manufacturer;
        $obj->Where = "where supplier_id=$supplier_id";
        return $obj->ListOfAllRecords();
    }

}

class supplier_allowed_manufacturer_brands extends cwebc {

    function __construct() {
        parent::__construct('supplier_allowed_manufacturer_brands');
        $this->requiredVars = array(
            'id',
            'supplier_id',
            'manufacturer',
            'brand'
        );
    }

    public static function isAllowedManufacturerBrand($manufacturer, $brand) {
        global $supplier_id;
        $obj = new supplier_allowed_manufacturer_brands;
        $obj->Where = " WHERE manufacturer='$manufacturer' and brand='$brand' AND supplier_id=$supplier_id";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : FALSE;
    }

    function getAllowedBrands($manufacturer) {
        global $supplier_id;
        $this->Where = " WHERE manufacturer='$manufacturer' AND supplier_id=$supplier_id";
        $data = $this->ListOfAllRecords();
        return !empty($data) ? $data : FALSE;
    }

}

class supplier_product_images extends cwebc {

    function __construct() {
        parent::__construct('supplier_product_images');
        $this->requiredVars = array(
            'id',
            'supplier_id',
            'sku',
            'image',
            'from_feed',
            'is_default',
            'is_deleted',
            'date_add',
        );
    }

    static function hasTrashedImages($sku) {
        global $supplier_id;
        $obj = new supplier_product_images();
        $obj->Where = " WHERE supplier_id=$supplier_id AND sku='$sku' AND is_deleted=1";
        $data = $obj->DisplayOne();
        return is_object($data) ? TRUE : FALSE;
    }

    static function isExistingImage($sku, $image) {
        global $supplier_id;
        $obj = new supplier_product_images();
        $obj->Field = "id";
        $obj->Where = " WHERE supplier_id=$supplier_id AND sku='$sku' AND image='$image'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : FALSE;
    }

    static function deleteImage($sku, $image) {
        if ($id = supplier_product_images::isExistingImage($sku, $image)) {
            //update is_deleted
            $obj = new supplier_product_images;
            $obj->Data['id'] = $id;
            $obj->Data['is_deleted'] = 1;
//            $this->print = 1;
            return $obj->Update();
        } else {
            //add new with is_deleted
            global $supplier_id;
            $obj = new supplier_product_images;
            $obj->Data['supplier_id'] = $supplier_id;
            $obj->Data['sku'] = $sku;
            $obj->Data['image'] = $image;
            $obj->Data['is_deleted'] = 1;
            return $obj->Insert();
        }
    }

    static function addImage($sku, $image) {
        global $supplier_id;
        $obj = new supplier_product_images;
        $obj->Data['supplier_id'] = $supplier_id;
        $obj->Data['sku'] = $sku;
        $obj->Data['image'] = $image;
        $obj->Data['from_feed'] = '0';
        return $obj->Insert();
    }

    static function restoreImage($sku, $image) {
        global $supplier_id;
        if ($id = supplier_product_images::isExistingImage($sku, $image)) {
            if (supplier_product_images::isFeedImage($id)) {
                //delete from table
                $obj = new supplier_product_images;
                return $obj->purgeObject($id);
            }
            //update is_deleted
            $obj = new supplier_product_images;
            $obj->Data['id'] = $id;
            $obj->Data['is_deleted'] = 0;
            return $obj->Update();
        }
    }

    static function isImageDeleted($sku, $image) {
        global $supplier_id;
        $obj = new supplier_product_images;
        $obj->Field = 'id';
        $obj->Where = " WHERE supplier_id=$supplier_id AND image='$image' AND sku='$sku' AND is_deleted=1";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : FALSE;
    }

    static function isFeedImage($id) {
        $obj = new supplier_product_images;
        $obj->Field = 'id';
        $obj->Where = " WHERE id=$id AND from_feed=1";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : FALSE;
    }

    function getSupplierProductImages($for_live_feed = FALSE) {
        global $supplier_id;
        global $account;
        $this->Where = " WHERE supplier_id=$supplier_id AND from_feed=0";
        if ($for_live_feed === TRUE) {
            $this->Where .= " AND is_deleted=0";
        }
        $data = array();
        foreach ($this->ListOfAllRecords() as $image) {
            $data[$image['sku']][] = DIR_WS_SITE . 'feed_images/' . $account->folder_name . '/product/large/' . $image['image'];
        }
        return $data;
    }

    public static function getDeletedImages($only_feed = FALSE) {
        $obj = new supplier_product_images;
        $obj->Where = " WHERE is_deleted=1";
        if ($only_feed === TRUE) {
            $obj->Where .= " AND from_feed=1";
        }
        $data = $obj->ListOfAllRecords();
        if (!empty($data)) {
            $deleted = [];
            foreach ($data as $del) {
                $deleted[$del['sku']][] = $del['image'];
            }
            $data = $deleted;
        }
        return $data;
    }

}

class supplier_default_commission extends cwebc {

    function __construct() {
        parent::__construct('supplier_default_commission');
        $this->requiredVars = array(
            'id',
            'supplier_id',
            'commission',
            'start_date'
        );
    }

    function saveGlobalCommission($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {

            $this->Insert();
            return $this->GetMaxId();
        }
    }

    public static function getGlobalCommission($date = '') {
        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }
        global $supplier_id;
        $obj = new supplier_default_commission;
        $obj->Where = "where supplier_id='$supplier_id' and start_date<='$date' order by id desc limit 1";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->commission : 10;
    }

    public static function checkCurrentkDateExists($date) {
        global $supplier_id;
        $obj = new supplier_default_commission;
        $obj->Where = "where supplier_id='$supplier_id' and start_date='$date'";
        return $obj->DisplayOne();
    }

}

class supplier_store_commission extends cwebc {

    function __construct() {
        parent::__construct('supplier_store_setting');
        $this->requiredVars = array(
            'id',
            'supplier_id',
            'store_id',
            'commission_percent',
            'is_shipping_free',
            'start_date'
        );
    }

    /* Save Commission Details
     * 
     */

    function saveCommission($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {

            $this->Insert();
            return $this->GetMaxId();
        }
    }

    //Check Website Commission Exists
    function getWebsiteCommission($website_id, $date = '') {
        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }
        global $supplier_id;
        $this->Where = "where supplier_id='$supplier_id' and store_id='$website_id' and start_date<='$date' order by id desc limit 1";
        $data = $this->DisplayOne();
        return is_object($data) ? $data->commission_percent : FALSE;
    }

    function getCommissionDetailsByStoreID($store_id) {
        global $supplier_id;
        $date = date('Y-m-d');
        $this->Where = "where supplier_id='$supplier_id' and store_id='$store_id' AND start_date<='$date' ORDER BY id DESC limit 1";
        return $this->DisplayOne();
    }

    public static function existingDateSetting($date) {
        global $supplier_id;
        $obj = new supplier_store_commission();
        $obj->Field = "id";
        $obj->Where = " WHERE `supplier_id`='$supplier_id' AND start_date='$date'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : FALSE;
    }

}

class supplier_website_payment extends cwebc {

    function __construct() {
        parent::__construct('supplier_website_payment');
        $this->requiredVars = array('id', 'supplier_id', 'website_id', 'payment', 'payment_method', 'date_add');
    }

    // Save Payment
    function savePayment($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    // List Payment
    function listPayments($website_id) {
        global $supplier_id;
        $this->Where = "where supplier_id='$supplier_id' and website_id='$website_id'";
        return $this->ListOfAllRecords();
    }

    // Get Payment Detail By Id
    public static function getPaymentById($id) {
        global $supplier_id;
        $obj = new supplier_website_payment;
        $obj->Where = "where id='$id' and supplier_id='$supplier_id'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : FALSE;
    }

    // Get Last Payment Date
    public static function getLastPaymentDate($website_id) {
        global $supplier_id;
        $obj = new supplier_website_payment;
        $obj->Where = "where supplier_id='$supplier_id' and website_id='$website_id' order by date_add desc limit 1";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->date_add : FALSE;
    }

}

class payment_method extends cwebc {

    function __construct() {
        parent::__construct('payment_method');
        $this->requiredVars = array('id', 'payment_method', 'folder_name', 'is_active', 'description', 'mode');
    }

    // List All Active Payment Methods
    function listAllPaymentMethods() {
        $this->Where = "where is_active='1'";
        return $this->ListOfAllRecords();
    }

}

class supplier_payment_methods extends cwebc {

    function __construct() {
        parent::__construct('supplier_payment_methods');
        $this->requiredVars = array('id', 'supplier_id', 'payment_method_id', 'is_active', 'date_add');
    }

    // Save Supplier Payment Methods
    function saveSupplierPaymentMethod($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    public static function countAllActivePAymentMethods() {
        $obj = new supplier_payment_methods;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where is_active='1'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    //Check Payment Method Exists
    public static function checkPaymentMethodExists($default_payment_method_id) {
        global $supplier_id;
        $obj = new supplier_payment_methods;
        $obj->Where = "where supplier_id='$supplier_id' and payment_method_id='$default_payment_method_id'";
        return $obj->DisplayOne();
    }

    //Check Supplier Payment Method Exists
    public static function checkSupplierPaymentMethodsExists() {
        global $supplier_id;
        $obj = new supplier_payment_methods;
        $obj->Where = "where supplier_id='$supplier_id'";
        return $obj->DisplayOne();
    }

    // List All Active Payment Methods
}

class supplier_payment_method_settings extends cwebc {

    function __construct() {
        parent::__construct('supplier_payment_method_settings');
        $this->requiredVars = array('id', 'supplier_payment_method_id', 's_key', 's_value', 'date_add');
    }

    // Save Supplier Payment Method Settings
    function saveSupplierPaymentMethodSettings($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    //Check Payment Method Settings Exists
    public static function settingsExists($supplier_payment_method_id, $s_key) {
        $obj = new supplier_payment_method_settings;
        $obj->Where = "where s_key='$s_key' and supplier_payment_method_id='$supplier_payment_method_id'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : FALSE;
    }

}

class supplier_setting extends cwebc {

    function __construct($show_fields = FALSE) {
        parent::__construct('supplier_setting');
        $Query = "SHOW COLUMNS FROM `$this->TableName`";
        $this->ExecuteQuery($Query);
        if ($this->GetNumRows()) {
            while ($row = $this->GetObjectFromRecord()) {
                $this->requiredVars[] = $row->Field;
            }
        }
        if ($show_fields === TRUE) {
            die(pr($this->requiredVars));
        }
    }

    function getSetting($key = '', $value_only = FALSE) {
        global $supplier_id;
        $this->Where = "WHERE supplier_id='$supplier_id'";
        if ($key) {
            $this->Where .= " AND `key`='$key'";
            $data = $this->DisplayOne();
            return is_object($data) ? ($value_only === TRUE ? $data->value : $data) : FALSE;
        }
        $data = $this->ListOfAllRecords('');
        return !empty($data) ? $data : FALSE;
    }
}

?>