<?php
/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's Management System
 */

class subscriptionPlan extends cwebc {
    
    function __construct() {
        parent::__construct('subscription_plan');
        $this->requiredVars=array('id','title','title','description','period_in_months','monthly_cost','total_emails','file_storage','is_active','is_free','is_deleted','on_date','update_date','position');
     }
     
    # Save Subscription
    function saveSubscriptionPlan($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
		$this->Data['is_free']=isset($this->Data['is_free'])?'1':'0';
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){ 
            if($this->Update())
              return $Data['id'];
        }
        else{  
            $this->Data['on_date']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }
	
	# List All Subscription
	function listSubscriptionPlan($show_active=0, $result_type='object'){
			if($show_active)
				$this->Where="where is_deleted='0' and is_active='1' ORDER BY position";    
			else
				$this->Where="where is_deleted='0' ORDER BY position";    
			if($result_type=='object')
				return $this->DisplayAll();        
			else
				return $this->ListOfAllRecords('object');        
	} 
	
	# List All Deleted Subscription
	function listDeletedSubscriptionPlan($result_type='object'){
			
			$this->Where="where is_deleted='1' ORDER BY position";   
			   
			if($result_type=='object')
				return $this->DisplayAll();        
			else
				return $this->ListOfAllRecords('object');        
	} 
	
    
	#get Subscription
	function getSubscriptionPlan($id){ 
            return $this->_getObject('subscription_plan', $id);
	} 
    
	# Restore Subscription
	function restoreServicePlan($id){
            $this->Data['is_active']='1';
            $this->Data['is_deleted']='0';
            $this->Data['id']=$id;
            $this->Update();
            return $id;
        }
	
	#get Delete Subscription
	function deleteServicePlan($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }
	
    #get Subscription Name
    function getSubscriptionNamePlan($id){
        $this->Field='id,title';
        $this->Where=" where id='".$id."'";
        return $this->DisplayOne();  
    } 	
    
    #get count Subscription 
    function getCountSubscription(){
        $this->Field='count(*) as total';
        $this->Where="where is_active='1' and is_deleted!='1' and is_free!='1'";
        $rec= $this->DisplayOne();  
        if($rec):
            return $rec->total;
        else:
            return '0';
        endif;
    }     
}
?>