<?php

/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's Management System
 */

class websiteSubscription extends cwebc {

    function __construct() {
        parent::__construct('website_subscription');
        $this->requiredVars = array('id', 'website_id', 'plan_id', 'total_emails', 'period_in_months', 'from_date', 'to_date', 'amount', 'payment_method', 'next_payment_date', 'on_date', 'update_date', 'is_trial', 'is_current');
    }

    # Save Subscription

    function saveWebisteSubscription($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_trial'] = isset($this->Data['is_free']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    # Save Free Website Subscription

    function createFeeWebsiteSubscription($site_id, $plan_id) {

        #Get Particular Plan
        $QueryObj = new subscriptionPlan();
        $plan = $QueryObj->getSubscriptionPlan($plan_id);

        #now save subscription
        $webSub = new websiteSubscription();
        $webSub->Data['is_trial'] = '1';
        $webSub->Data['website_id'] = $site_id;
        $webSub->Data['period_in_months'] = $plan->period_in_months;
        $webSub->Data['total_emails'] = $plan->total_emails;
        $webSub->Data['plan_id'] = $plan_id;
        $webSub->Data['on_date'] = date('Y-m-d');
        $webSub->Data['from_date'] = date('Y-m-d');
        $webSub->Data['to_date'] = date("Y-m-d", strtotime(date('Y-m-d') . " +" . $plan->period_in_months . " months"));
        $webSub->Insert();
        return $webSub->GetMaxId();
    }

    # Save Website Subscription

    function createWebsiteSubscription($site_id, $plan_id) {

        #Get Particular Plan
        $QueryObj = new subscriptionPlan();
        $plan = $QueryObj->getSubscriptionPlan($plan_id);

        #now save subscription
        $webSub = new websiteSubscription();
        $webSub->Data['is_trial'] = '1';
        $webSub->Data['website_id'] = $site_id;
        $webSub->Data['period_in_months'] = $plan->period_in_months;
        $webSub->Data['total_emails'] = $plan->total_emails;
        $webSub->Data['amount'] = $plan->monthly_cost;
        $webSub->Data['is_current'] = '1';
        $webSub->Data['plan_id'] = $plan_id;
        $webSub->Data['on_date'] = date('Y-m-d');
        $webSub->Data['from_date'] = date('Y-m-d');
        $webSub->Data['to_date'] = date("Y-m-d", strtotime(date('Y-m-d') . " +1 months"));

        $webSub->Insert();
        return $webSub->GetMaxId();
    }

    # Upgrade Website upgradeWebsiteSubscription

    function upgradeWebsiteSubscription($current, $plan) {

        #Get Particular Plan
        $QueryObj = new subscriptionPlan();
        $plan = $QueryObj->getSubscriptionPlan($plan);

        #Get current website subscription
        $QueryObj1 = new websiteSubscription();
        $current_sub = $QueryObj1->getWebsiteSubscription($current);



        #now save subscription
        $webSub = new websiteSubscription();
        $webSub->Data['is_trial'] = '0';
        $webSub->Data['website_id'] = $current_sub->website_id;
        $webSub->Data['period_in_months'] = $plan->period_in_months;
        $webSub->Data['amount'] = $plan->monthly_cost;
        $webSub->Data['is_current'] = '0';
        $webSub->Data['total_emails'] = $plan->total_emails;

        $webSub->Data['plan_id'] = $plan->id;
        $webSub->Data['on_date'] = date('Y-m-d');
        $webSub->Data['from_date'] = date('Y-m-d');
        $webSub->Data['to_date'] = date("Y-m-d", strtotime(date('Y-m-d') . " +1 months"));

        $webSub->Insert();
        $max_id = $webSub->GetMaxId();

        #Set Other plan not current 
        //$QueryObj12 = new websiteSubscription();
        //$QueryObj12->setOtherSubscriptionNotCurrent($webSub->GetMaxId(),$current_sub->website_id);

        return $max_id;
    }

    # set all other not current

    function setOtherSubscriptionNotCurrent($current_id, $website_id) {
        $web = new query('website_subscription');
        $web->Where = "where website_id='" . $website_id . "' and id!='" . $current_id . "'";
        //$web->print=1;
        $record = $web->ListOfAllRecords('object');
        if (!empty($record)):
            foreach ($record as $key => $val):
                #update subscription
                $webSub = new websiteSubscription();
                $webSub->Data['id'] = $val->id;
                $webSub->Data['is_current'] = '0';
                $webSub->Update();
            endforeach;
        else:
            return true;
        endif;
    }

    # Update Website Subscription

    function countWebsiteSubscription($website_id) {
        $this->Field = 'count(*) as count';
        $this->Where = "where website_id='" . $website_id . "'";
        $record = $this->DisplayOne();
        if ($record):
            return $record->count;
        else:
            return '0';
        endif;
    }

    # Update Website Subscription

    function updateWebsiteSubscription($id) {
        #Get Particular Plan
        $QueryObj = new websiteSubscription();
        $sub = $QueryObj->getWebsiteSubscription($id);

        #Set Other plan not current 
        $QueryObj1 = new websiteSubscription();
        $QueryObj1->setOtherSubscriptionNotCurrent($sub->id, $sub->website_id);
        #now save subscription
        $webSub = new websiteSubscription();
        $webSub->Data['is_trial'] = '0';
        $webSub->Data['id'] = $id;
        $webSub->Data['from_date'] = date('Y-m-d');
        $webSub->Data['is_current'] = '1';
        $webSub->Data['to_date'] = date("Y-m-d", strtotime(date('Y-m-d') . " +" . $sub->period_in_months . " months"));
        $webSub->Data['next_payment_date'] = date("Y-m-d", strtotime(date('Y-m-d') . " +" . $sub->period_in_months . " months"));
        //$webSub->print=1;
        $webSub->Update();
        return $id;
    }

    #get Subscription

    function getWebsiteSubscription($id) {
        return $this->_getObject('website_subscription', $id);
    }

    #get Users All Website With Subscription	

    function getAllWebsitesWithUserInfo($user_id) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,subscription_plan as subscription');
        $web->Field = 'user_website.website_name,user_website.domain_name,user_website.is_active as website_active,subscription.title,website_subscription.*';
        $web->Where = "where user_website.user_id='" . $user_id . "' AND user_website.id=website_subscription.website_id AND website_subscription.is_current='1' AND subscription.id=website_subscription.plan_id";
        //$web->print=1;
        return $web->ListOfAllRecords('object');
    }

    #get Users Randomly Single Website With Subscription	

    function getSingleWebsitesWithUserInfo($user_id) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,subscription_plan as subscription');
        $web->Field = 'user_website.website_name,user_website.domain_name,user_website.database_hostname,user_website.database_name,user_website.database_user,user_website.database_password,subscription.title,website_subscription.*';
        $web->Where = "where user_website.user_id='" . $user_id . "' AND user_website.id=website_subscription.website_id AND website_subscription.is_current='1' AND subscription.id=website_subscription.plan_id ORDER BY RAND() limit 0,1";

        return $web->DisplayOne();
    }

    #get Delete Subscription

    function deleteService($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    # get Monthly Trial & Regular Graph

    function getMonthlyTrialRegularGraph($month, $year) {
        if (strlen($month == '1')) {
            $month = '0' . $month;
        }

        $last_date_of_month = date("d", strtotime("+1 month -1 second", strtotime(date($year . "-" . $month . "-1"))));

        $rec = array();
        $trial = array();

        $graph = '';

        #get Regular Sites
        $query = new query('website_subscription');
        $query->Field = 'id,day(on_date) AS month_day, count(*) as total';
        $query->Where = "where YEAR(on_date)='" . $year . "' AND MONTH(on_date)='" . $month . "' AND is_current='1' AND is_trial!='1' GROUP BY on_date";
        $record = $query->ListOfAllRecords('object');

        if (!empty($record)):
            foreach ($record as $key => $value):
                $rec[$value->month_day] = $value->total;
            endforeach;
        endif;


        ###############################################################
        #################   Now get Trial Website #####################
        ###############################################################         
        #get Regular Sites
        $query = new query('website_subscription');
        $query->Field = 'id,day(on_date) AS month_day, count(*) as total';
        $query->Where = "where YEAR(on_date)='" . $year . "' AND MONTH(on_date)='" . $month . "' AND is_current='1' AND is_trial!='0' GROUP BY on_date";
        $record1 = $query->ListOfAllRecords('object');

        if (!empty($record1)):
            foreach ($record1 as $key1 => $value1):
                $trial[$value1->month_day] = $value1->total;
            endforeach;
        endif;

        $date_array = array('1' => '1st', '2' => '2nd', '3' => '3th', '4' => '4th', '5' => '5th', '6' => '6th', '7' => '7th', '8' => '8th', '9' => '9th', '10' => '10th', '11' => '11st', '12' => '12nd', '13' => '13th', '14' => '14th', '15' => '15th', '16' => '16th', '17' => '17th', '18' => '18th', '19' => '19th', '20' => '20th', '21' => '21st', '22' => '22nd', '23' => '23th', '24' => '24th', '25' => '25th', '26' => '26th', '27' => '27th', '28' => '28th', '29' => '29th', '30' => '30th', '31' => '31th');


        $i = '1';
        while ($i <= $last_date_of_month):

            if (array_key_exists($i, $rec)):
                $graph.="['" . $date_array[$i] . "'," . $rec[$i];
            else:
                $graph.="['" . $date_array[$i] . "'," . '0';
            endif;

            if (array_key_exists($i, $trial)):
                $graph.="," . $trial[$i] . "],";
            else:
                $graph.="," . '0' . "],";
            endif;

            $i++;
        endwhile;

        $graph = substr_replace(trim($graph), "", -1);

        return $graph;
    }

    function getWebsiteCurrentSubscription($website_id) {
        $this->Where = "where website_id = " . $website_id . " and is_current=1";
        //$this->print=1;
        $record = $this->DisplayOne();
        return $record;
    }

    function change_currentstateof_unusedplan($id) {
        $this->Where = " WHERE website_id=" . $_SESSION["website_id"] . " AND id!=" . $id;
        $this->Data["is_current"] = "0";
        //$this->UpdateCustom();
    }

}

?>