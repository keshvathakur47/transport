<?php

/*
 * zone Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class zone extends cwebc {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'desc', $orderby = 'name') {
        parent::__construct('supplier_zone');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'name', 'is_active', 'is_deleted', 'date_add');
    }

    /*
     * Create new zone or update existing zone
     */

    function saveZone($POST) {
        global $supplier_id;
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['supplier_id'] = $supplier_id;
        $this->Data['is_active'] = isset($POST['is_active']) ? "1" : "0";
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * Get zone by id
     */

    function getZone($id) {
        global $supplier_id;
        $this->Where = " where id = '$id' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }

    /*
     * Get List of all zone
     */

    function listZones($rtype = '', $active = false) {
        global $supplier_id;
        if ($active):
            $this->Where.=" where supplier_id='$supplier_id' and is_active='1' and is_deleted='0' order by $this->orderby $this->order";
        else:
            $this->Where.=" where supplier_id='$supplier_id' and is_deleted='0' order by $this->orderby $this->order";
        endif;
        if ($rtype == 'array'):
            return $this->ListOfAllRecords();
        else:
            return $this->DisplayAll();
        endif;
    }

    public static function getZoneIdByCountry($country) {
        $obj = new zone;
        $obj->Where = "where name='$country'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->id : '0';
    }

}

class zone_detail extends cwebc {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'desc', $orderby = 'id') {
        parent::__construct('supplier_zone_detail');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'zone_id', 'country_id', 'state_id', 'city_id', 'zipcode', 'is_active', 'is_deleted', 'date_add');
    }

    /*
     * Create new zone or update existing zone
     */

    function saveZoneDetails($zone_id, $POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        global $supplier_id;
        $this->Data['supplier_id'] = $supplier_id;
        $this->Data['zone_id'] = $zone_id;
        $this->Data['is_active'] = isset($POST['is_active']) ? "1" : "0";
        if ($this->Data['state_id'] == ''):
            $this->Data['state_id'] = '-1';
        endif;
        if ($this->Data['city_id'] == ''):
            $this->Data['city_id'] = '-1';
        endif;
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            //$this->print=1;
            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * Get zone by id
     */

    function getZoneDetails($id) {
        global $supplier_id;
        $this->Where = " where `id` = $id and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }

    /*
     * Get List of all zone details
     */

    function listZoneDetails($zone_id) {
        global $supplier_id;
        $query = new query('supplier_zone_detail as czd');
        $query->Field = "cct.name as city,cs.name as state,cc.short_name as country,czd.* ";
        $query->Where = " left join " . TABLE_PREFIX . "supplier_country as cc on czd.country_id=cc.id";
        $query->Where.=" left join " . TABLE_PREFIX . "supplier_state as cs on czd.state_id=cs.id";
        $query->Where.=" left join " . TABLE_PREFIX . "supplier_city as cct on czd.city_id=cct.id";
        $query->Where.=" where czd.supplier_id='$supplier_id' and czd.zone_id='$zone_id' and czd.is_deleted='0' order by czd.id desc";
        return $query->ListOfAllRecords();
    }

    /*
     * get zone rule by address
     */

    public static function getZoneByAddress($cart = '') {
        if ($cart == ''):
            $cart = cart::getCart();
        endif;
        $zone_id = '0';
        if (isset($cart->shipping_zip) && $cart->shipping_zip != '' && $cart->shipping_zip):
            $query = new zone_detail();
            $zone_id = $query->getZoneByZip($cart->shipping_zip, $cart->shipping_country);
        endif;
        if ($zone_id == '0' && isset($cart->shipping_city) && $cart->shipping_city != ''):
            $query1 = new zone_detail();
            $zone_id = $query1->getZoneByCityName($cart->shipping_city, $cart->shipping_country);
        endif;
        if ($zone_id == '0' && isset($cart->shipping_state) && $cart->shipping_state != ''):
            $query2 = new zone_detail();
            $zone_id = $query2->getZoneByStateid($cart->shipping_state, $cart->shipping_country);
        endif;
        if ($zone_id == '0' && isset($cart->shipping_country) && $cart->shipping_country != ''):
            $query3 = new zone_detail();
            $zone_id = $query3->getZoneByCountryid($cart->shipping_country);
        endif;
        return $zone_id;
    }

    /*
     * get zone by country id
     */

    function getZoneByCountryid($country_id) {
        global $supplier_id;
        $this->Where = " where supplier_id='$supplier_id' and country_id='$country_id' AND `is_active` = '1' AND `is_deleted` = '0'";
        $this->Where.=" and state_id='-1' and city_id='-1' and zipcode='' order by id asc";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object->zone_id;
        else:
            return '0';
        endif;
    }

    /*
     * get zone by state id
     */

    function getZoneByStateid($state_id, $country_id = '') {
        global $supplier_id;
        $this->Where = " where supplier_id='$supplier_id' and state_id='$state_id' AND `is_active` = '1' AND `is_deleted` = '0'";
        if ($country_id != ''):
            $this->Where.=" and country_id='$country_id'";
        endif;
        $this->Where.=" and city_id='-1' and zipcode='' order by id asc";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object->zone_id;
        else:
            return '0';
        endif;
    }

    /*
     * get zone by zipcode
     */

    function getZoneByZip($zipcode, $country_id) {
        global $supplier_id;
        $this->Where = " where supplier_id='$supplier_id' and LOWER(zipcode) LIKE '%" . strtolower($zipcode) . "%' AND country_id='$country_id' AND `is_active` = '1' AND `is_deleted` = '0' order by id asc";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object->zone_id;
        else:
            return '0';
        endif;
    }

    /*
     * get zone by city
     */

    function getZoneByCityName($city_name, $country_id = '') {
        $query_name = new city();
        $city_id = $query_name->getCityIdByName($city_name, $country_id);
        if ($city_id):
            global $supplier_id;
            $query = new zone_detail();
            $query->Where = " where supplier_id='$supplier_id' and city_id='$city_id' AND `is_active` = '1' AND `is_deleted` = '0'";
            if ($country_id != ''):
                $query->Where.=" and country_id='$country_id'";
            endif;
            $query->Where.=" and zipcode='' order by id asc";
            $object = $query->DisplayOne();
            if (is_object($object)):
                return $object->zone_id;
            endif;
        endif;
        return '0';
    }

}

?>