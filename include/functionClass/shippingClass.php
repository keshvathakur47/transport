<?php

/*
 * Shipping Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class shipping extends cwebc {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    function __construct($table = 'supplier_shipping', $order = 'asc', $orderby = 'position') {
        parent::__construct($table);
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'name', 'desc', 'logo', 'handling_charges', 'tracking_url', 'is_active', 'is_deleted', 'is_default', 'sequence', 'position', 'date_add');
    }

    /*
     * list all shipping services
     */

    function listShipping($active = false, $rtype = '') {
        global $supplier_id;
        $this->Where = " where supplier_id='$supplier_id' ";

        if ($active):
            $this->Where.= " and is_active='1'";
        endif;

        $this->Where.= " and is_deleted='0' order by $this->orderby $this->order";

        if ($rtype == 'array'):
            return $this->ListOfAllRecords();
        else:
            return $this->DisplayAll();
        endif;
    }

    /*
     * get shipping method
     */

    function getShippingMethod($id) {
        global $supplier_id;
        $this->Where = " where id='$id' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }

    /* calculate shipping as per cart */

    public static function calculate_shipping() {
        if (IS_SHIPPING_FREE):
            return "Free Shipping";
        else:
            $Query = new shipping();
            $default_shipping = $Query->getDefaultShipping();


        endif;
    }

    /* include all active shipping classes */

    public static function includeShippingMethodClasses() {
        $QueryObj = new shipping();
        $Active_shipping_methods = $QueryObj->listShipping(false, 'array');
        if (!empty($Active_shipping_methods)):
            foreach ($Active_shipping_methods as $kk => $vv):
                include_once(DIR_FS_SITE . 'include/functionClass/shipping' . ucfirst($vv['code']) . 'Class.php');
            endforeach;
        endif;
    }

    /* get default shipping */

    function getDefaultShipping() {
        global $supplier_id;
        $QueryObj = new shipping;
        $QueryObj->Where = " where is_default='1' and supplier_id='$supplier_id'";
        return $QueryObj->DisplayOne();
    }

    /*
     * get all available shipping methods on checkout page
     * as per address detail in cart
     */

    public static function getAvailableShippingMethods($zone_id = '0', $cart = '', $checkout_page = true) {
        if ($cart == ''):
            $cart = cart::getCart();
        endif;
        if ($zone_id == '0'):
            $zone_id = zone_detail::getZoneByAddress($cart);
        endif;

        $methods = array();

        if (!IS_SHIPPING_FREE && $cart->coupon_free_shipping == 0 && ($cart->is_shipping_address_saved || !$checkout_page) && $zone_id != '0'):

            $QueryObj = new shipping();
            $Active_shipping_methods = $QueryObj->listShipping(TRUE, 'array');

            if (!empty($Active_shipping_methods)):
                foreach ($Active_shipping_methods as $kk => $vv):
                    //$zone_shipping_method = '';

                    $class_obj_name = 'shipping_' . $vv['code'];
                    $QueryObj1 = new $class_obj_name();
                    $zone_shipping_method = $QueryObj1->calculateShippingByZone($zone_id);

                    if (count($zone_shipping_method)):
                        foreach ($zone_shipping_method as $kkk => $vvv):
                            if (!$vvv["is_shipping_free"] && $vvv["total_shipping_amount"] > 0):
                                $zone_shipping_method[$kkk]['total_shipping_amount'] = $vvv['total_shipping_amount'] + $vv['handling_charges'];
                            endif;
                            $methods[] = $zone_shipping_method[$kkk];
                        endforeach;
                    endif;
                endforeach;
            endif;

            if (!empty($methods)):
                if ($checkout_page):
                    $Query = new cart();
                    if (!$Query->checkShippingMethodSaved($cart->id)):
                        $Query = new cart();
                        $Query->saveShippingDetailsCart($methods['0']);
                    endif;
                endif;
                return $methods;
            endif;
        endif;

        if ($checkout_page):
            $Query = new cart();
            $Query->resetShippingMethodDetails($cart->id);
        endif;

        return false;
    }

    /*
     * update handling fee for shippings
     */

    function updateHandlingFee($shipping) {
        global $supplier_id;
        if (is_array($shipping) && !empty($shipping)):
            foreach ($shipping as $kk => $vv):
                $Query = new shipping();
                $Query->Data['id'] = $kk;
                $Query->Data['supplier_id'] = $supplier_id;
                $Query->Data['handling_charges'] = $vv;
                $Query->Update();
            endforeach;
        endif;
        return;
    }

}

class shipping_group extends shipping {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * construct for calling class table and required fields
     */

    function __construct($table = 'supplier_shipping_group', $order = 'asc', $orderby = 'position') {
        parent::__construct($table);
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'shipping_method', 'name', 'delivery_time', 'description', 'date_add', 'date_upd', 'position', 'min_delivery_days', 'max_delivery_days');
    }

    /*
     * list all shipping groups
     */

    function listShippingGroups($shipping_method, $return = 'object') {
        global $supplier_id;
        $this->Where = " where supplier_id = '$supplier_id' and shipping_method = '$shipping_method' order by position asc";
        if ($return == 'array'):
            return $this->ListOfAllRecords('object');
        else:
            return $this->DisplayAll();
        endif;
    }

    /*
     * add new shippping group
     */

    function updateShippingGroup($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * get shipping group
     */

    function getShippingGroup($id) {
        global $supplier_id;
        $this->Where = " where id='$id' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }

    /*
     * get group delivery time from rule id
     */

    public static function getShippingGroupApproxTime($id) {
        global $supplier_id;
        $query = new query('supplier_shipping_group');
        $query->Field = "delivery_time";
        $query->Where = " where id='$id' and supplier_id='$supplier_id'";
        $object = $query->DisplayOne();
        if (is_object($object)):
            return $object->delivery_time;
        else:
            return false;
        endif;
    }

}
