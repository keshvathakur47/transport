<?php

/* * **************** output buffering started *********** */
ob_start();

/* * **************** set error level ******************** */
error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors', 1);

/* * **************** start session by default *********** */
session_start();

/* ###############################################################################################
 * ####################     Website developed by: cWebConsultatns.com       ######################
 * ############################################################################################# */

/*
 * *******************************************************
 *        MAKE CHANGES TO THE FOLLOWING CODE 
 * *******************************************************
 */

/* set charset */
mb_internal_encoding("UTF-8");
include_once("server_config.php");
if ($is_local == 1):
    //Server path
    define("HTTP_SERVER", "http://" . $_SERVER['HTTP_HOST'], true);
    define("HTTP_SERVER_SSL", "https://" . $_SERVER['HTTP_HOST'], true);

    //Set website 
    define("DIR_WS_SITE", HTTP_SERVER . "/transport/", true);
    define("DIR_WS_SITE_SSL", HTTP_SERVER_SSL . "/transport/", true);
else:
    // Server path
    if (strstr($_SERVER['HTTP_HOST'], 'heroicstores') || strstr($_SERVER['HTTP_HOST'], 'sextoys')):
        define("HTTP_SERVER", "http://www.heroicstores.com", true);
        define("HTTP_SERVER_SSL", "https://www.heroicstores.com", true);
    else:
        define("HTTP_SERVER", "http://qtx.in", true);
        define("HTTP_SERVER_SSL", "https://qtx.in", true);
    endif;
    // Set website 
    define("DIR_WS_SITE", HTTP_SERVER_SSL . "/", true);
    define("DIR_WS_SITE_SSL", HTTP_SERVER_SSL . "/", true);

endif;
/* * **************** Database Settings *************** */
require_once('database.php');

/* * *********** EMAIL SMTP AUTHENTICATION *************** */
define("SMTP_EMAIL", false);

/* * **************** Admin Folder Name *************** */
define("ADMIN_FOLDER", 'admin');

/* * ******** theme settings *********************** */
define('CURRENT_THEME', 'default');
define('THEME_PATH', DIR_WS_SITE . 'theme/' . CURRENT_THEME, true);
/*
 * *******************************************************
 *        DO NOT MAKE ANY CHANGE BELOW THIS LINE 
 * *******************************************************
 */
/* * ******** Set Website Filesystem ********* */
define("DIR_FS", $_SERVER['DOCUMENT_ROOT'] . '/', true);
define("DIR_FS_SITE", dirname(dirname(dirname(__FILE__))) . '/', true);

require_once(DIR_FS_SITE . 'include/config/import_file_constants.php');
/* * ********** SESSION NAME ***************** */
define("ADMIN_SESSION_NAME", 'admin_session_trans', true);
define("FRONT_USER_SESSION_NAME", 'user_session_trans', true);

/* * **** create automatic tables ****************** */
include_once(DIR_FS_SITE . 'include/functionClass/tableClass.php');

/* include sub-configuration files here */
require_once(DIR_FS_SITE . "include/config/url.php");

/* include the database class files */
require_once(DIR_FS_SITE_INCLUDE_CLASS . "mysql.php");
require_once(DIR_FS_SITE_INCLUDE_CLASS . "query.php");

/* include the utitlity files here */
require_once(DIR_FS_SITE_INCLUDE_CLASS . "phpmailer.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG . "constant.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG . "message.php");

/* custom files */
include_once(DIR_FS_SITE_INCLUDE_CLASS . 'login_session.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS . 'admin_session.php');

/* include functions here */
include_once(DIR_FS_SITE_INCLUDE_FUNCTION . 'date.php');


/* include function files here */
include_once(DIR_FS_SITE . 'include/function/basic.php');

include_once(DIR_FS_SITE . 'include/functionClass/imageManipulationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/fileClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/dateClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailClass.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS . 'errorClass.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS . "validation_u.php");
include_once(DIR_FS_SITE_INCLUDE_CLASS . "validation_p.php");

if (strstr($_SERVER['HTTP_HOST'], 'heroicstores')):
    if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""):
        $redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        Redirect1($redirect);
    endif;
endif;
/* include smarty class */
require_once(DIR_FS_SITE . 'include/libs/Smarty.class.php');
require_once(DIR_FS_SITE . 'include/class/smartyClass.php');
?>