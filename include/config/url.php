<?php
# you will find the urls to main sections of the website here.
# Website URLs.
define("DIR_WS_SITE_GRAPHIC", DIR_WS_SITE."theme/".CURRENT_THEME."/graphic/",true);
define("DIR_WS_SITE_UPLOAD", DIR_WS_SITE."upload/",true);
define("DIR_WS_SITE_INCLUDE", DIR_WS_SITE."include/",true);
define("DIR_WS_SITE_CSS", DIR_WS_SITE."theme/".CURRENT_THEME."/css/",true);
define("DIR_WS_SITE_PHP", DIR_WS_SITE."php/");
define("DIR_WS_SITE_TEMPLATE", DIR_WS_SITE."view/".CURRENT_THEME."/",true);
define("DIR_WS_SITE_THEME", DIR_WS_SITE."theme/".CURRENT_THEME."/",true);
define("DIR_WS_SITE_JAVASCRIPT",DIR_WS_SITE."theme/".CURRENT_THEME."/javascript/",true);
define("DIR_WS_SITE_CONTROL", DIR_WS_SITE.ADMIN_FOLDER."/",true);
define("DIR_WS_SITE_ADMIN", DIR_WS_SITE.ADMIN_FOLDER."/",true);

define("DIR_WS_SITE_CONTROL_INCLUDE", DIR_WS_SITE_CONTROL."include/",true);
define("DIR_WS_SITE_CONTROL_IMAGE", DIR_WS_SITE_CONTROL."image/",true);
define("DIR_WS_SITE_CONTROL_PHP", DIR_WS_SITE_CONTROL."script/",true);
define("DIR_WS_SITE_CONTROL_FORM", DIR_WS_SITE_CONTROL."form/",true);
define("DIR_WS_SITE_CONTROL_CSS", DIR_WS_SITE_CONTROL."css/",true);

# Graphic URLs.
define("DIR_WS_SITE_GRAPHIC_ICON", DIR_WS_SITE_GRAPHIC."icon/",true);
define("DIR_WS_SITE_GRAPHIC_TITLE", DIR_WS_SITE_GRAPHIC."title/",true);
define("DIR_WS_SITE_GRAPHIC_BUTTON", DIR_WS_SITE_GRAPHIC."button/",true);
define("DIR_WS_SITE_GRAPHIC_FLASH", DIR_WS_SITE_GRAPHIC."flash/",true);
define("DIR_WS_SITE_GRAPHIC_IMAGE", DIR_WS_SITE_GRAPHIC."image/",true);
define("DIR_WS_SITE_GRAPHIC_EDGE", DIR_WS_SITE_GRAPHIC."edge/",true);

#Upload URLs  |Use these paths to upload files| 
define("DIR_WS_SITE_UPLOAD_PHOTO", DIR_WS_SITE_UPLOAD."photo/",true);
define("DIR_WS_SITE_UPLOAD_VIDEO", DIR_WS_SITE_UPLOAD."video/",true);
define("DIR_WS_SITE_UPLOAD_AUDIO", DIR_WS_SITE_UPLOAD."audio/",true);

define("DIR_WS_SITE_UPLOAD_PHOTO_CATEGORY", DIR_WS_SITE_UPLOAD_PHOTO."category/",true);
define("DIR_WS_SITE_UPLOAD_PHOTO_PRODUCT", DIR_WS_SITE_UPLOAD_PHOTO."product/",true);
define("DIR_WS_SITE_UPLOAD_PHOTO_BANNER", DIR_WS_SITE_UPLOAD_PHOTO."banner/",true);

#Include URLs
define("DIR_WS_SITE_INCLUDE_CLASS", DIR_WS_SITE_INCLUDE."class/",true);
define("DIR_WS_SITE_INCLUDE_CONFIG", DIR_WS_SITE_INCLUDE."config/",true);
define("DIR_WS_SITE_INCLUDE_EMAIL", DIR_WS_SITE_INCLUDE."email/",true);
define("DIR_WS_SITE_INCLUDE_EXCEL", DIR_WS_SITE_INCLUDE."excel/",true);
define("DIR_WS_SITE_INCLUDE_FUNCTION", DIR_WS_SITE_INCLUDE."function/",true);
define("DIR_WS_SITE_INCLUDE_NEWSLETTER", DIR_WS_SITE_INCLUDE."newsletter/",true);
define("DIR_WS_SITE_INCLUDE_TEMPLATE", DIR_WS_SITE_INCLUDE."template/",true);
define("DIR_WS_SITE_INCLUDE_TEST_TOOL", DIR_WS_SITE_INCLUDE."test_tool/",true);
define("DIR_WS_SITE_INCLUDE_PAYMENT_GATEWAY", DIR_WS_SITE_INCLUDE."payment_gateway/",true);


# File System URLs

define("DIR_FS_SITE_GRAPHIC", DIR_FS_SITE."theme/".CURRENT_THEME."/graphic/",true);
define("DIR_FS_SITE_UPLOAD", DIR_FS_SITE."upload/",true);
define("DIR_FS_SITE_INCLUDE", DIR_FS_SITE."include/",true);
define("DIR_FS_SITE_CSS", DIR_FS_SITE."theme/".CURRENT_THEME."/css/",true);
define("DIR_FS_SITE_PHP", DIR_FS_SITE."php/");
define("DIR_FS_SITE_JAVASCRIPT", DIR_FS_SITE."theme/".CURRENT_THEME."/javascript/",true);
define("DIR_FS_SITE_CONTROL", DIR_FS_SITE.ADMIN_FOLDER."/",true);
define("DIR_FS_SITE_ADMIN", DIR_FS_SITE.ADMIN_FOLDER."/",true);
define("DIR_FS_SITE_TEMPLATE", DIR_FS_SITE."view/".CURRENT_THEME."/",true);

define("DIR_FS_SITE_CONTROL_INCLUDE", DIR_FS_SITE_CONTROL."include/",true);
define("DIR_FS_SITE_CONTROL_IMAGE", DIR_FS_SITE_CONTROL."image/",true);
define("DIR_FS_SITE_CONTROL_PHP", DIR_FS_SITE_CONTROL."script/",true);
define("DIR_FS_SITE_CONTROL_FORM", DIR_FS_SITE_CONTROL."form/",true);
define("DIR_FS_SITE_CONTROL_CSS", DIR_FS_SITE_CONTROL."css/",true);

# Graphic URLs.
define("DIR_FS_SITE_GRAPHIC_ICON", DIR_FS_SITE_GRAPHIC."icon/",true);
define("DIR_FS_SITE_GRAPHIC_TITLE", DIR_FS_SITE_GRAPHIC."title/",true);
define("DIR_FS_SITE_GRAPHIC_BUTTON", DIR_FS_SITE_GRAPHIC."button/",true);
define("DIR_FS_SITE_GRAPHIC_FLASH", DIR_FS_SITE_GRAPHIC."flash/",true);
define("DIR_FS_SITE_GRAPHIC_IMAGE", DIR_FS_SITE_GRAPHIC."image/",true);
define("DIR_FS_SITE_GRAPHIC_EDGE", DIR_FS_SITE_GRAPHIC."edge/",true);

#Upload URLs
define("DIR_FS_SITE_UPLOAD_PHOTO", DIR_FS_SITE_UPLOAD."photo/",true);
define("DIR_FS_SITE_UPLOAD_VIDEO", DIR_FS_SITE_UPLOAD."video/",true);
define("DIR_FS_SITE_UPLOAD_AUDIO", DIR_FS_SITE_UPLOAD."audio/",true);

define("DIR_FS_SITE_UPLOAD_PHOTO_CATEGORY", DIR_FS_SITE_UPLOAD_PHOTO."category/",true);
define("DIR_FS_SITE_UPLOAD_PHOTO_PRODUCT", DIR_FS_SITE_UPLOAD_PHOTO."product/",true);
define("DIR_FS_SITE_UPLOAD_PHOTO_BANNER", DIR_FS_SITE_UPLOAD_PHOTO."banner/",true);

#Include URLs
define("DIR_FS_SITE_INCLUDE_CLASS", DIR_FS_SITE_INCLUDE."class/",true);
define("DIR_FS_SITE_INCLUDE_CONFIG", DIR_FS_SITE_INCLUDE."config/",true);
define("DIR_FS_SITE_INCLUDE_EMAIL", DIR_FS_SITE_INCLUDE."email/",true);
define("DIR_FS_SITE_INCLUDE_EXCEL", DIR_FS_SITE_INCLUDE."excel/",true);
define("DIR_FS_SITE_INCLUDE_FUNCTION", DIR_FS_SITE_INCLUDE."function/",true);
define("DIR_FS_SITE_INCLUDE_NEFSLETTER", DIR_FS_SITE_INCLUDE."neFSletter/",true);
define("DIR_FS_SITE_INCLUDE_TEMPLATE", DIR_FS_SITE_INCLUDE."template/",true);
define("DIR_FS_SITE_INCLUDE_TEST_TOOL", DIR_FS_SITE_INCLUDE."test_tool/",true);
define("DIR_FS_SITE_INCLUDE_PAYMENT_GATEWAY", DIR_FS_SITE_INCLUDE."payment_gateway/",true);


#Smarty URLs
define("DIR_FS_SITE_SMARTY_CACHE", DIR_FS_SITE."cache/",true);
define("DIR_FS_SITE_SMARTY_COMPILED", DIR_FS_SITE."compiled/",true);
define("DIR_FS_SITE_SMARTY_FUNCTION", DIR_FS_SITE_INCLUDE.'functionSmarty/',true);
define("DIR_FS_SITE_SMARTY_CACHE_PATH", DIR_FS_SITE."cache",true);
define("DIR_FS_SITE_SMARTY_COMPILED_PATH", DIR_FS_SITE."compiled",true);
?>