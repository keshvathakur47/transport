<?php
/* WEBSITE CONSTANTS */
$constants=new query('setting');
$constants->DisplayAll();
while($constant=$constants->GetObjectFromRecord()):
	define("$constant->key", $constant->value, true);
endwhile;

/*put these constants in admin panel under settings*/
define('MAXIMUM_META_TITLE_LENGHT', 60, true);
define('MAXIMUM_META_DESC_LENGHT', 160, true);


//define('SITE_NAME', "e-commerce", true);
define('MAIN_NAVIGATION_ID', 1, true);
define('BOTTOM_NAVIGATION_ID', 2, true);
define('SITE_TAG_LINE','Made with <small><span class="glyphicon glyphicon-heart tertiary-color"></span></small> for you.', true);
define('BOTTOM_LEFT_TEXT','© Copyright 2014.');
define('BOTTOM_RIGHT_TEXT','Made with <span class="glyphicon glyphicon-heart"></span> by <a class="footer__link" href="http://www.cwebconsultants.com/" target="_blank">cWebconsultants</a>');
define("SITE_CACHE_STATUS",FALSE);
define("SITE_DEBUG_STATUS",FALSE);
define("SLIDER_ACTIVE",1,true);//TRUE OR FALSE
define("SHOW_SLIDER_ON_PAGES","home",true);//HOME OR ALL
define("SHOW_SLIDER_ON_MOBILE","home",true);//TRUE OR FALSE
define("LOGO_PATH","http://cweb9/budgell/theme/default/graphic/logo.png",true);
define("SHOW_CURRENCY_ON_TOP",1,true);//TRUE OR FALSE
define("SHOW_LANGUAGE_ON_TOP",1,true);//TRUE OR FALSE
if (!defined('MASTER_ADMIN_EMAIL')) {
    define('MASTER_ADMIN_EMAIL', 'info@heroicstores.com');
}

/* PHP Validation types*/
define('VALIDATE_REQUIRED', "req", true);
define('VALIDATE_EMAIL',"email", true);
define("VALIDATE_MAX_LENGTH","maxlength");
define("VALIDATE_MIN_LENGTH","minlength");
define("VALIDATE_NUMERIC","num");
define("VALIDATE_ALPHA","alpha");
define("VALIDATE_ALPHANUM","alphanum");
define("SUBJECT_NEW_ORDER","Order");
define("SUBJECT_NEW_ORDER_CUSTOMER","Customer Order");
define("TEMPLATE","default");
define("CURRENCY_SYMBOL", "$");

define("ADD_ATTRIBUTE_PRICE_TO_PRODUCT_PRICE", true);
define("ATTRIBUTE_PRICE_OVERLAP", false);

define('VERIFY_EMAIL_ON_REGISTER', true);
define('ERROR_EMAIL', 'rocky.developer004@gmail.com', true);

$conf_shipping_type=array('quantity', 'subtotal');

define('SHIPPING_TYPE', 'price');		/* price/quantity/weight */

define("STOCK_THRESHOLD", 3);
define("REVIEW_FOR_BUYER", 0);
define("REVIEW_FOR_USER", 1);

$AllowedImageTypes=array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$AllowedFileTypes=array('application/vnd.ms-excel');

/* new allowed photo mime type array.*/
$conf_allowed_photo_mime_type=array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$conf_order_status=array('Received', 'Processing', 'Shipped', 'Delivered');

define("DOC_TYPE", '<!DOCTYPE html>', true);
define("DOC_LANGUAGE", "en", true);
define("DOC_CHAR_SET", 'utf-8', true);

$not_to_open_page = array ();

$imageThumbConfig=array(
	'brand'=>array(
                    'thumb'=>array('width'=>'50','height'=>'50'),
                    'small'=>array('width'=>'190', 'height'=>'140'), 
                    'medium'=>array('width'=>'458', 'height'=>'458'),
                    'big'=>array('width'=>'800', 'height'=>'600')
         ),
        'supplier'=>array(
                    'thumb'=>array('width'=>'50','height'=>'50'),
                    'small'=>array('width'=>'190', 'height'=>'140'), 
                    'medium'=>array('width'=>'458', 'height'=>'458'),
                    'big'=>array('width'=>'800', 'height'=>'600')
         ),
        'category'=>array(
                    'thumb'=>array('width'=>'50','height'=>'50'),
                    'small'=>array('width'=>'190', 'height'=>'140'), 
                    'medium'=>array('width'=>'458', 'height'=>'458'),
                    'big'=>array('width'=>'800', 'height'=>'600')
         ),
        'product'=>array(
                    'thumb'=>array('width'=>'50','height'=>'50'),
                    'small'=>array('width'=>'190', 'height'=>'140'), 
                    'medium'=>array('width'=>'458', 'height'=>'458'),
                    'big'=>array('width'=>'800', 'height'=>'600')
         ),
	
);

/** defining tables for status on - off in admin as per module name**/
$moduleTableForStatus=array(
       'domain' => 'website_domain'
);
?>