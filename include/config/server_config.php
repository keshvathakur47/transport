<?php

$is_local = 0;
if (strstr($_SERVER['HTTP_HOST'], 'heroicstores') || strstr($_SERVER['HTTP_HOST'], 'sextoys') || strstr($_SERVER['HTTP_HOST'], 'qtx')) {
    if (strstr($_SERVER['HTTP_HOST'], 'heroicstores') || strstr($_SERVER['HTTP_HOST'], 'sextoys')) {
        define("SERVER_NAME", "heroicstores", true);
        define("DIR_WS_SITE_MAIN", "https://www.heroicstores.com/");
    } else {
        define("SERVER_NAME", "qtx", true);
        define("DIR_WS_SITE_MAIN", "http://qtx.in/");
    }
    list($first, $second) = explode(".", $_SERVER["SERVER_NAME"]);
    $is_subdomain = 0;
    $subdomain = "";

    if ($first == "www"):
        if ($second != SERVER_NAME):
            $is_subdomain = 1;
            $subdomain = $second;
        endif;

    elseif ($first != SERVER_NAME):
        $is_subdomain = 1;
        $subdomain = $first;
    endif;
    $site_folder = $subdomain;
} else {
    $is_local = 1;
    define("DIR_WS_SITE_MAIN", "http://localhost/transport/");
}