<?php
class cart 
{
	var $cart_id;
	var $currency;
	var $check_stock_flag;
	var $check_stock_with_attribute_flag;
	var $add_vat_flag;
	var $vat_percent;
	var $allow_repeat_product_flag;
	var $allow_buy_if_out_of_stock;
	var $attribute_price_overlap;
	var $add_attribute_price_to_product_price;
	var $enable_voucher_code;
	var $enable_product_attribute;
			
	function cart()
	{
		if(!isset($_SESSION['online_cart'])):
			$_SESSION['online_cart']=array();
			$_SESSION['online_cart']['id']=session_id();
			$_SESSION['online_cart']['billing_address']=array();
			$_SESSION['online_cart']['shipping_address']=array();
			$_SESSION['online_cart']['shipping']=array();
			$_SESSION['online_cart']['currency']='GBP';
			$this->cart_id=session_id();
		endif;
		$this->check_stock_with_attribute_flag=CHECK_STOCK_WITH_ATTRIBUTE;	
		$this->check_stock_flag=CART_STOCK;
		$this->add_vat_flag=CART_VAT;
		$this->vat_percent=CART_VAT_PERCENT;
		$this->allow_repeat_product_flag=ALLOW_REPEAT_PRODUCT;
		$this->allow_buy_if_out_of_stock=ALLOW_BUY_IF_OUT_OF_STOCK;
		$this->enable_voucher_code=ENABLE_VOUCHER_CODE;
		$this->enable_product_attribute=USE_PRODUCT_ATTRIBUTE;
	}
	

	
	function get_cart_id()
	{
		if(isset($_SESSION['online_cart']['id'])):
			return $_SESSION['online_cart']['id'];
		else:
			return 0;
		endif;
	}	
	
	function regenerate_cart_id()
	{
		session_regenerate_id();
	}
	
	function is_cart_exist()
	{
		$query= new query('cart');
		$query->Where="where cart_id='$this->cart_id'";
		$query->DisplayAll();
		if($query->GetNumRows()):
			return true;
		else:
			return false;
		endif;
	}
	
	function update_in_cart($item_id,$quantity)
	{			
			$cart_item= get_object('cart', $item_id);
			$cart_item->product_id;
			$product= get_object('product', $cart_item->product_id);
			$product->stock; 
			if($product->stock>=$quantity): 
					if(get_object('cart', $item_id)):			
						$query= new query('cart');
						$query->Data['id']=$item_id;
						$query->Data['quantity']=$quantity;
						$query->Update();						
						return true;
					endif;
			else:
			Redirect(make_url('cart','&msg=Currently there is only '.$product->stock.' in stocks'));
			endif;
	}	
	
		/* Check Newslatter Subscription  */	
        function is_subscribed($user_id)
        {	
		$object= get_object('user', $user_id);
		$query=new query('newsletterlist');		
		$query->Where="where user_id='$user_id'";
		$test=$query->DisplayOne();
		if($test):
			if($test->newsletter=='1'):
				return $Yes=1;
			else:
				return $Yes=0;
			endif;	
		else:
			return $Yes=0;
		endif;			
		}
		
	/* Newslatter Subscription  */	
        function subscribe_newslatter($user_id)
        {	
		$object= get_object('user', $user_id);
	$query1=new query('newsletterlist');		
	$query1->Where="where user_id='$user_id'";
	$object1=$query1->DisplayOne();	
		
		$query=new query('newsletterlist');
		$query->Data['user_id']=$user_id;
		$query->Data['name']=$object->firstname." ".$object->lastname;
		$query->Data['email']=$object->username;		
		$query->Data['newsletter']='1';
		//$query->Data['city']=$object->city;
		//$query->print=1;
		if($object1):
			$query->Data['id']=$object1->id;
			$query->Update();
		else:
			$query->Insert();
		endif;	
		return true;
		}	

	/* Newslatter Unsubscription  */	
        function unsubscribe_newslatter($user_id)
        {	
		$object= get_object('user', $user_id);
		$query=new query('newsletterlist');		
		$query->Where="where user_id='$user_id'";
		$object=$query->DisplayOne();
		
		//print_r($object); exit;
		if($query->GetNumRows()):
			$query=new query('newsletterlist');	
			$query->Data['id']=$object->id;
			$query->Data['newsletter']='0';
			$query->Update();
			return true;
		endif;
		}		
	
	function add_to_cart($method='POST')
	{
		$log= new user_session();
		#fetch product details from table.
		$id=$this->get_value($method, 'id');
		$product=get_object('product', $id);
		$string='';
                $att_price=0;
                if($this->get_value($method, 'attribute')):
                    foreach ($this->get_value($method, 'attribute') as $key=>$value):
						//$att=get_object('attribute', $key);
                        //$att_value=get_object('attribute_value', $value);
                        $string.=trim(strtolower($value)).',';
                        //$attributes.=$att->name.'/'.$att_value->name.'/'.$att_value->price.'<br/>';
                     endforeach;
                    $string=substr($string, 0, strlen($string)-1);
				endif;
                $query= new query('attribute_value');
                $query->Where="where product_id='$id' and name='$string'";
                $object=$query->DisplayOne();
                
		#stock checking on attribute level
		if($this->check_stock_with_attribute_flag):
			if($this->get_value($method, 'attribute')):
				if(!$this->check_attribute_stock($string, $id, $this->get_value($method, 'quantity'))):
					if($this->allow_buy_if_out_of_stock):
						$log->pass_msg[]='Warning: Item with particular attribute is out of stock.';
						$log->set_pass_msg();
					else:
						$log->pass_msg[]='Warning: Item with particular attribute is out of stock.';
						$log->set_pass_msg();
						Redirect(make_url('pdetail', 'id='.$this->get_value($method, 'id')));
					endif;
				endif;
			endif;
                #stock checking on the product level.
		elseif($this->check_stock_flag):
			if(!$this->check_product_stock($product->id, $this->get_value($method, 'quantity'), $method)):
				if($this->allow_buy_if_out_of_stock):
					$log->pass_msg[]='Warning: Item is currently not in stock.';
					$log->set_pass_msg();
					#set message and do not redirect.
				else:
					$log->pass_msg[]='Warning: Item is currently not in stock.';
					$log->set_pass_msg();
					Redirect(make_url('pdetail', 'id='.$this->get_value($method, 'id')));
				endif;
			endif;
		endif;
		
                #add product to cart table.
		$query= new query('cart');
		$query->Data['cart_id']=$this->get_cart_id();
		$query->Data['product_id']=$this->get_value($method, 'id');
		$query->Data['quantity']=$this->get_value($method, 'quantity');
		$query->Data['on_date']=date("Y-m-d");
		$query->Data['ip_address']=$_SERVER['REMOTE_ADDR'];
		$query->Data['product_name']=$product->name;
               
                #insert attribute details.
//		if($this->get_value($method, 'attribute')):
//                    foreach ($this->get_value($method, 'attribute') as $key=>$value):
//		  	$att=get_object('attribute', $key);
//                        $att_value=get_object('attribute_value', $value);
//                        $attributes.=$att->name.'/'.$att_value->name.'/'.$att_value->price.'<br/>';
//                        $att_price+=$att_value->price;
//                    endforeach;
//		endif;
                $query->Data['attribute']=$string;
               	if(is_object($object) && $object->price>0):
					$query->Data['price']=$object->price;
				else:
					$query->Data['price']=$this->get_value($method, 'price');
				endif;
                $query->Insert();
	}

	function apply_shipping_cost($code, $total, $user_id)
	{
	//echo $code; exit;
		$shipping_obj=new query('delivery');
		$shipping_obj->Where="where zipcode like '%".$code."%'";
		//$shipping_obj->print=1;
		$shipping_value=$shipping_obj->DisplayOne();
		if($shipping_value):
			$object= get_object_by_col('cart_more_detail', 'cart_id', $this->get_cart_id());
			if($object):
			//print_r($shipping_value); exit;
				$query= new query('cart_more_detail');							
					$query->Data['shipping_amount']=$shipping_value->price;
					$query->Data['shipping_comment']=$code;					
					$query->Data['user_id']=$user_id;
					$query->Data['id']=$object->id;
					//$query->print=1;
					$query->Update();
			else:
				$query= new query('cart_more_detail');				
					$query->Data['shipping_amount']=$shipping_value->price;
					$query->Data['shipping_comment']=$code;	
					$query->Data['cart_id']=$this->get_cart_id();
					$query->Data['user_id']=$user_id;
					$query->Insert();				
			endif;
			return true;
		else:
			return false;
		endif; 
	
	}	






	
	function remove_from_cart($item_id)
	{
		if(get_object('cart', $item_id)):
			#delete item
			$query= new query('cart');
			$query->id=$item_id;
			$query->Delete();
			#delete attributes
			$query= new query('cart_attribute');
			$query->Where="where cart_instance_id='$item_id'";
			$query->Delete_where();
		endif;
		#set message & redirect
		Redirect(make_url('cart'));
	}
	
	function empty_cart()
	{
		if(isset($_SESSION['online_cart'])):
			unset($_SESSION['online_cart']);
		endif;
		$this->regenerate_cart_id();
		return true;
	}
	
	function recalculate($item_id, $quantity, $method='POST')
	{
		$cart_item= get_object('cart', $item_id);
		$log= new user_session();
		#check attribute stock;
		#stock checking on attribute level
		
		if($this->check_stock_with_attribute_flag):
			if(!$this->check_attribute_stock($cart_item->attibute, $cart_item->product_id, $quantity)):
                            if($this->allow_buy_if_out_of_stock):
				$log->pass_msg[]='Warning: Item is currently not in stock.';
				$log->set_pass_msg();
                            else:
				$log->pass_msg[]='Warning: Item is currently not in stock.';
				$log->set_pass_msg();
				Redirect(make_url('cart'));
                            endif;
			endif;
		elseif($quantity>$cart_item->quantity):
			if($this->check_stock_flag):
				if(!$this->check_product_stock($cart_item->product_id, $quantity, $method)):
					if($this->allow_buy_if_out_of_stock):
						$log->pass_msg[]='Warning: Item is currently not in stock.';
						$log->set_pass_msg();
					else:
						$log->pass_msg[]='Warning: Item is currently not in stock.';
						$log->set_pass_msg();
						Redirect(make_url('cart'));
					endif;
				endif;
			endif;
		endif;

                $query= new query('cart');
		$query->Data['id']=$cart_item->id;;
		$query->Data['quantity']=$quantity;
		$query->Update();
	}
	
	function get_item_from_cart($id)
	{
		return get_object('cart', $id);
	}
		
	function get_cart_total()
	{
		$total=0;
		$cart_id=$this->get_cart_id();
		$query= new query('cart');
		$query->Where="where cart_id='$cart_id'";
		$query->DisplayAll();		
		while ($object=$query->GetObjectFromRecord()) :
			$total+=$this->get_cart_item_total($object->id);
		endwhile;
		return $total;
	}
	
//	function get_attribute_total()
//	{
//		$att_total=0;
//		$cart_id=$this->get_cart_id();
//		$query= new query('cart');
//		$query->Where="where cart_id='$cart_id'";
//		$query->DisplayAll();
//		while($object= $query->GetObjectFromRecord()):
//				$query1= new query('cart_attribute');
//				$query1->Where="where cart_instance_id='$object->id' and is_paid_attribute=1";
//				$query1->Field="sum(price*$object->quantity) as total";
//				$ob=$query1->DisplayOne();
//				$att_total+=$ob->total;
//		endwhile;
//		return $att_total;
//	}
	
	function get_cart_vat()
	{
                $amount=$this->get_cart_total();
		if($this->add_vat_flag):
			return ($this->vat_percent*$amount)/100;
		else:
			return 0;
		endif;
	}

        # for shopping cart page (without shipping charges)
        function get_grand_total(){
            $cart_total=$this->get_cart_total();
            $cart_vat=$this->get_cart_vat();
            $cart_voucher=$this->get_coupon_value();
            $shipping= $this->get_cart_shipping();
            return ($cart_total+$cart_vat+$shipping)-$cart_voucher;
        }


	function get_cart_total_items()
	{
		$cart_id=$this->get_cart_id();
		$query= new query('cart');
		$query->Field="sum(quantity) as qty";
		$query->Where="where cart_id='$cart_id'";
		if($total=$query->DisplayOne()):
			return $total->qty;
		endif;
		return 0;
	}
	
	function check_product_stock($id, $quantity, $method='post')
	{
		$product=get_object('product', $id);
		$quantity=($quantity)?$quantity:1;
		if($quantity > $product->stock):
			return false;
		else:
			return true;
		endif;
		return true;
	}
	
	function check_attribute_stock($string, $product_id, $quantity)
	{
		$query= new query('attribute_value');
                $query->Where="where product_id='$product_id' and name='$string'";
                $object=$query->DisplayOne();
                if(is_object($object)):
                        if($object->stock>=$quantity):
                             return true;
                        else:
                             return false;
                        endif;
                endif;
		return false;
	}
	
	function get_cart_item_total($id)
	{
		$total=0;
		$object=get_object('cart', $id);
		return $object->price*$object->quantity;
	}
	
//	function get_cart_item_attribute_total($id)
//	{
//		$item=get_object('cart', $id);
//		$object= new query('cart_attribute');
//		$object->Where="where cart_instance_id='$item->id'";
//		$object->DisplayAll();
//		$total=0;
//		while($obj= $object->GetObjectFromRecord()):
//			if($obj->is_paid_attribute):
//				$total+=$obj->price*$item->quantity;
//			endif;
//		endwhile;
//		return $total;
//	}
	
	/* billing functions */
	
	function is_billing_set()
	{
		$query =new query('address');
		$cart_id=$this->get_cart_id();
		$query->Where="where cart_id='$cart_id' and address_type='bill'";
		if($obj=$query->DisplayOne()):
			return $obj->id;
		else:	
			return false;
		endif;		
	}
	
	function set_billing($billing_address=array())
	{
		$login= new user_session();
		
		$query =new query('address');
		$query->Data['first_name']=isset($billing_address['first_name'])?$billing_address['first_name']:'none';
		$query->Data['last_name']=isset($billing_address['last_name'])?$billing_address['last_name']:'none';
		$query->Data['phone']=isset($billing_address['phone'])?$billing_address['phone']:'none';
		$query->Data['email']=isset($billing_address['email'])?$billing_address['email']:'';
		$query->Data['address1']=isset($billing_address['address1'])?$billing_address['address1']:'none';
		$query->Data['address2']=isset($billing_address['address2'])?$billing_address['address2']:'none';
		$query->Data['city']=isset($billing_address['city'])?$billing_address['city']:'none';
		$query->Data['zip']=isset($billing_address['zip'])?$billing_address['zip']:'none';
		$query->Data['state']=isset($billing_address['state'])?$billing_address['state']:'none';
		$query->Data['country']=isset($billing_address['country'])?$billing_address['country']:'none';
		$query->Data['fax']=isset($billing_address['fax'])?$billing_address['fax']:'none';
		$query->Data['address_type']='bill';
		$query->Data['user_id']=$login->is_logged_in()?$login->get_user_id():0;
		$query->Data['cart_id']=$this->get_cart_id();
		$cart_id=$this->get_cart_id();
		
		if($id=$this->is_billing_set()):
			$query->Data['id']=$id;
			$query->Update();
		else:
			$query->Insert();
		endif;
		
		return true;
	}
	
	/*function get_billing_address()
	{
		$query =new query('address');
		$cart_id= $this->get_cart_id();
		$query->Where="where cart_id='$cart_id' and address_type='bill'";
		if($obj=$query->DisplayOne('array')):
			return $obj;
		else:	
			return false;
		endif;		
	}
	*/
	
	function get_billing_address($user_id)
	{
		$query =new query('address');
		
		$query->Where="where user_id='$user_id' and address_type='bill'";
		if($obj=$query->DisplayOne()):
			return $obj;
		else:	
			return false;
		endif;		
	}
	
	
	function get_shipping_address($user_id)
	{
		$query =new query('address');
		
		$query->Where="where user_id='$user_id' and address_type='ship'";
		if($obj=$query->DisplayOne()):
			return $obj;
		else:	
			return false;
		endif;		
	}
	
	function save_billing_address()
	{
			$q= new query('address');
			$q->Where="where cart_id='$this->cart_id' and address_type='bill'";
			if($object=$q->DisplayOne()):
				$query= new query('address');
				$query->Data['use_later']=1;
				$query->Data['id']=$object->id;
				$query->Update();
			endif;
			return true;
	}
	
	/* shipping functions */
	
	function is_shipping_set()
	{
		$query =new query('address');
		$cart_id= $this->get_cart_id();
		$query->Where="where cart_id='$cart_id' and address_type='ship'";
		if($obj=$query->DisplayOne()):
			return $obj->id;
		else:	
			return false;
		endif;		
	}
	
	function set_shipping($shipping_address=array())
	{
		$login= new user_session();
		$query =new query('address');
		$query->Data['first_name']=isset($shipping_address['first_name'])?$shipping_address['first_name']:'none';
		$query->Data['last_name']=isset($shipping_address['last_name'])?$shipping_address['last_name']:'none';
		$query->Data['phone']=isset($shipping_address['phone'])?$shipping_address['phone']:'none';
		$query->Data['email']=isset($shipping_address['email'])?$shipping_address['email']:'none@none.com';
		$query->Data['address1']=isset($shipping_address['address1'])?$shipping_address['address1']:'none';
		$query->Data['address2']=isset($shipping_address['address2'])?$shipping_address['address2']:'none';
		$query->Data['city']=isset($shipping_address['city'])?$shipping_address['city']:'none';
		$query->Data['zip']=isset($shipping_address['zip'])?$shipping_address['zip']:'none';
		$query->Data['fax']=isset($shipping_address['fax'])?$shipping_address['fax']:'none';
		$query->Data['state']=isset($shipping_address['state'])?$shipping_address['state']:'none';
		$query->Data['country']=isset($shipping_address['country'])?$shipping_address['country']:'none';
		$query->Data['address_type']='ship';
		$query->Data['user_id']=$login->is_logged_in()?$login->get_user_id():0;
		$query->Data['cart_id']=$this->get_cart_id();
		$cart_id= $this->get_cart_id();
		if($id=$this->is_shipping_set()):
			$query->Data['id']=$id;
			$query->Update();
		else:
			$query->Insert();
		endif;
		return true;
	}
				
	function get_cart_shipping()
	{
            $query = new query('cart_more_detail');
            $cart_id=$this->get_cart_id();
            $query->Where="where cart_id='$cart_id'";
            $object=$query->DisplayOne();
            if(is_object($object)):
                    return $object->shipping_amount;
            endif;
            return 0;
	}
	
	/*function get_shipping_address()
	{
		$query =new query('address');
		$cart_id= $this->get_cart_id();
		$query->Where="where cart_id='$cart_id' and address_type='ship'";
		if($obj=$query->DisplayOne('array')):
			return $obj;
		else:	
			return false;
		endif;		
	}*/
	
	function cal_shipping_by_id($id)
	{
		
	}
	
	function save_shipping_address()
	{
			$q= new query('address');
			$q->Where="where cart_id='$this->cart_id' and address_type='ship'";
			if($object=$q->DisplayOne()):
				$query= new query('address');
				$query->Data['use_later']=1;
				$query->Data['id']=$object->id;
				$query->Update();
			endif;
			return true;
	}
	
	/*functions for more table */
	
	function get_cart_more_xyz($attribute)
	{
		$query =new query('cart_more_detail');
		$query->Where="where cart_id='$this->cart_id'";
		$object=$query->DisplayOne();
		return isset($object->$attribute)?$object->$attribute:'';
		
	}
		
	/* voucher related functions */
	
	function get_value($method='post', $key)
	{
		if(strtolower($method)=='post'):
			return (isset($_POST[$key]))?$_POST[$key]:false;
		elseif(strtolower($method)=='get'):
			return (isset($_GET[$key]))?$_GET[$key]:false;
		endif;
		return false;
	}

	function update_discount_cost($total)
	{
			$object= get_object_by_col('cart_more_detail', 'cart_id', $this->get_cart_id());
			if($object):
				if(($object->voucher_code!='') or ($object->voucher_code!='NULL')):
					$this->apply_coupon($object->voucher_code,$total);
					return true;
				else:
					return false;
				endif;				
			else:
				return false;
			endif;
	}		

	
	function apply_coupon($code, $total)
	{
		#get coupon object from database.
		$c_date=date("Y/m/d");
		$coupon_obj =new query('discount');
		$coupon_obj->Where="where code='$code' and min_sub_total<='$total' and cast('$c_date' as date) between from_date and to_date";
                //$coupon_obj->print=1;
		if(!($coupon=$coupon_obj->DisplayOne())):
			return false;
                else:
                     $this->cal_coupon_value($coupon, $total);
                     return true;
		endif;
		
		#check if applied to all
		/*if($coupon->apply_to_all):
			$this->cal_coupon_value($coupon, $total);
			return true;
		endif;
		*/
                /*
		#Now get all the categories & product ids from cart
		$category=array();
		$product=array();
		$test_q= new query('cart');
		$test_q->Where="where cart_id='$this->get_cart_id()'";
		$test_q->DisplayAll();
		while($obj= $test_q->GetObjectFromRecord()):
				$product[$obj->product_id]=$this->get_cart_item_total($obj->id);
				$category[$obj->product_id]= get_product_parent_id($obj->product_id);
		endwhile;
		$discount=0;
		# check for categories;
		if(count($category)):
			foreach ($category as $k=>$v):
				$query= new query('discount_category');
				$query->Where="where discount_id='$coupon_obj->id' and category_id='$v'";
				if($query->DisplayOne()):
					$discount+=$product[$k];
					unset($category[$k]);
					unset($product[$k]);
				endif;
			endforeach;
		endif;
		
		# check for remaining products
		if(count($product)):
			foreach ($product as $k=>$v):
				$query= new query('discount_product');
				$query->Where="where discount_id='$coupon_obj->id' and product_id='$k'";
				if($query->DisplayOne()):
					$discount+=$v;
					unset($product[$k]);
				endif;
			endforeach;
		endif;
		
		if($discount>0):
			$this->cal_coupon_value($coupon, $discount);
			return true;
		endif;
		*/
		return false;
	}	
	
	function cal_coupon_value($coupon_obj, $total)
	{
		switch ($coupon_obj->type):
		case '%-':
			$amt=($total*$coupon_obj->amount)/100;
			$object= get_object_by_col('cart_more_detail', 'cart_id', $this->get_cart_id());
			if(is_object($object)):
				$query= new query('cart_more_detail');
				$query->Data['voucher_code']=$coupon_obj->code;
				$query->Data['voucher_amount']=$amt;
				$query->Data['id']=$object->id;
				$query->Update();
			else:
				$query= new query('cart_more_detail');
				$query->Data['voucher_code']=$coupon_obj->code;
				$query->Data['voucher_amount']=$amt;
				$query->Data['cart_id']=$this->get_cart_id();
				$query->Insert();
			endif;
			break;
		case'-': 
		default:
			$amt=$coupon_obj->amount;
			$object= get_object_by_col('cart_more_detail', 'cart_id', $this->get_cart_id());
			if(is_object($object)):
				$query= new query('cart_more_detail');
				$query->Data['voucher_code']=$coupon_obj->code;
				$query->Data['voucher_amount']=$amt;
				$query->Data['id']=$object->id;
				$query->Update();
			else:
				$query= new query('cart_more_detail');
				$query->Data['voucher_code']=$coupon_obj->code;
				$query->Data['voucher_amount']=$amt;
				$query->Data['cart_id']=$this->get_cart_id();
				$query->Insert();
			endif;
			break;
		endswitch;
	}	
	
	function get_coupon_value()
	{
		$coupon=get_object_by_col('cart_more_detail', 'cart_id', $this->get_cart_id());
                if(is_object($coupon)):
                    return $coupon->voucher_amount;
                else:
                    return 0;
                endif;
	}
		
	function get_coupon_code()
	{
		$coupon=get_object('cart_more_detail', 'cart_id', $this->get_cart_id());
		return $coupon->voucher_code;
	}

	function cart_stock($id)
        {
                    if($this->check_stock_flag):
                            $query1= new query('product');
                            $query1->Where="where id='$id' and is_active=1";
                            $query1->Field="stock";
                            $ob=$query1->DisplayOne();
                            if(is_object($ob)):
                                    return true;
                            else:
                                    return false;
                            endif;
                    endif;
        }

function GetSHIP($st)
	{	
			if(SHIPPING_TYPE=='price')
				$weight=$this->get_cart_total();
			elseif(SHIPPING_TYPE=='quantity')
				$weight=$this->get_cart_total_items();
		    elseif(SHIPPING_TYPE=='weight')
				$weight=$this->get_cart_weight();
			
            $q=new query('shipping');
            $q->Where="where id='$st' and ('$weight'>=min and '$weight'<=max)";
            if($q1=$q->DisplayOne()){
                return $q1->amount;
            }
            else{
                $q=new query('shipping');
                $q->Where="where id='$st'";
                $q->DisplayAll();
                $object = $q->GetObjectFromRecord();
                return $object->amount+(($weight-$object->max)*$object->extra);
           }
		   return 0;
	}
        		
		
		function iscartexists()
	{
		$query= new query('cart');
		$query->Where="where cart_id='".$this->get_cart_id()."'";
		$query->DisplayAll();
		if($query->GetNumRows()>0):
			return true;
		else:
			return false;
		endif;
	}
	
	function GetItemTotal()
	{
		$query=new query('cart');
		$query->Field="sum(price*quantity) as total";
		$query->Where="where cart_id='".trim($this->get_cart_id(), 0)."'";
		if($total=$query->DisplayOne()):
			return $total->total;
		endif;
		return 0;	
	}	
	
	
	function GetVATFromPrice($percent=CART_VAT_PERCENT){
		
		
		$total=$this->get_cart_total();
		return $total-((100*$total)/(100+$percent));
		print_r($total);exit;
	}
	
	function GetPriceAfterVAT($percent=CART_VAT_PERCENT){
		$total=$this->get_cart_total();
		return (100*$total)/(100+$percent);
	}
	
	function deductVAT($amount, $percent=CART_VAT_PERCENT){
		return (100*$amount)/(100+$percent);
	}
	
	function get_cart_same_item_total($pid)
	{
		$cart_id=$this->get_cart_id();
		$query= new query('cart');
		$query->Field="sum(quantity) as qty";
		$query->Where="where cart_id='$cart_id' and product_id='$pid'";
		if($total=$query->DisplayOne()):
			return $total->qty;
		endif;
		return 0;
	}
	
	
	function get_cart_same_item_total_exist($pid)
	{
		$cart_id=$this->get_cart_id();
		$query= new query('cart');
		$query->Where="where cart_id='$cart_id' and product_id='$pid'";
		if($total=$query->DisplayOne()):
			return true;
		else:
			return false;
		endif;
	}
	
	  
        /* incase the shipping is calcualted on weight */
        
        function get_product_weight($id){
            $object= get_object('product', $id);
            return $object->weight;
        }
        
        function get_cart_weight(){
                $total=0;
		$cart_id=$this->get_cart_id();
		$query= new query('cart');
		$query->Where="where cart_id='$cart_id'";
		$query->DisplayAll();		
		while ($object=$query->GetObjectFromRecord()) :
			$total+=$this->get_product_weight($object->product_id)*$object->quantity;
		endwhile;
		return $total;
        }
	
	
	
};

$cart_obj = new cart();
?>