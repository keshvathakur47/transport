<?php 
/** 
 * Smarty plugin for ajax paging
 * 
 * @package Smarty 
 * @subpackage Plugin Paging
 * @author varun@cwebconsultants.com 
 */ 


function smarty_function_paging_ajax($params, &$smarty)
{
              
                $max_records=10;$menu_id=2;$url='';$querystring='';
                $page=$params['page'];
                $totalPages=$params['total_pages'];
                $totalRecords=$params['total_records'];
               
                
                 if(isset($params['query'])):
                   $querystring=$params['query'];
                 endif;
                 if(isset($params['url'])):
                   $url=$params['url'];
                 endif;
                 if(isset($params['max_records'])):
                   $max_records=$params['max_records'];
                 endif;
                 if(isset($params['menu_id'])):
                   $menu_id=$params['menu_id'];
                 endif;
           
                
	        /*Paging by Ajax*/
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
               
		?>

                 <!--Here in "id" we set page and in "rel" we set total pages , "max_records" here we set max records on a page ,"query_string" we pass query string "menu id" represents menu id--> 
                 <div class="row-fluid">
                          <div class="pagination pagination-centered">
                              <ul>
                                  <li><a href="javascript:void(0)" id="<?php echo $Pp ?>" rel="<?php echo $totalPages?>" max_records="<?php echo $max_records?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring?>" title="Previous Page" class="paging_change"  ><?php echo "«"?></a></li>
          
                              
                               <?
				for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
					
                                         if($i==$page):?>
                                               
                                                 <li>  <a href="javascript:void(0)" id="<?php echo $i ?>" rel="<?php echo $totalPages?>" max_records="<?php echo $max_records?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring?>" class="active paging_change"><?php echo $i ?></a></li>
                                              
					<?php else: ?>
						<li>  <a href="javascript:void(0)" id="<?php echo $i ?>" rel="<?php echo $totalPages?>" max_records="<?php echo $max_records?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring?>" class="paging_change"  ><?php echo $i ?></a></li>
                                                
					<?php endif;					
				endfor;
				?>
                  
                               <li><a href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages?>" max_records="<?php echo $max_records?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring?>" class="paging_change"  title="Next Page"  ><?php echo "»"?></a></li>
                              </ul>
                          </div>         
		</div>
 <?php          
}
?> 