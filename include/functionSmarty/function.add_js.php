<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.add_js.php
 * Type:        function
 * Name:        add_js
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - name         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * name= js name
 */

function smarty_function_add_js($params, &$smarty)
{  
    $array = array();
    if(!empty($params['name'])) 
       $array=explode(',',$params['name']); 
       if(count($array)):
           foreach ($array as $k=>$v):
                echo '<script src="'.DIR_WS_SITE_JAVASCRIPT.trim($v).'.js" type="text/javascript"></script> '."\n";
           endforeach;
       endif;
}
?>
