<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.head.php
 * Type:        function
 * Name:        head
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - name         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * just call this function withour parameters
 */
function smarty_function_head($params, &$smarty)
{  
       head(isset($content)?$content:'');   
}
?>
