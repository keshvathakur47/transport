<?php 
/** 
 * Smarty plugin to check whether a file or directory exists
 * 
 * @package Smarty 
 * @subpackage Plugin file_exists
 * @author varun@cwebconsultants.com 
 */ 



function smarty_function_file_exists($params, &$smarty)
{
            $filename = '';
            
            if(isset($params['path']) && !empty($params['path'])):
                $filename=$params['path'];    
            endif;

            if (file_exists($filename)) {
                return true;
            } else {
               return false;
            }
       
}	
?>