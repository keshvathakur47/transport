<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.get_iamge.php
 * Type:     function
 * Name:     get image
 * Purpose:  to get image of any size from specific folder
 * -------------------------------------------------------------
 */

function smarty_function_get_image($params, &$smarty)
{
    $type='gallery';
    $size='large';
    $image=$params['image'];
    
    if(isset($params['type']) && !empty($params['type'])):
     $type=$params['type'];    
    endif;
    
    if(isset($params['size']) && !empty($params['size'])):
     $size=$params['size'];    
    endif;
    
    $image_obj=new imageManipulation();
    return $image_obj->get_image($type,$size,$image);
  
   
}
?>