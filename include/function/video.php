<?php
function get_video_player($link, $echo=1)
{
	$firstpos=strpos($link, '=');
	$secondpos=strpos($link, '&');
       	if(!$secondpos or $secondpos==''):
		$code=substr($link, $firstpos+1);
	else:
                $length=$secondpos-($firstpos+1);
		$code=substr($link, $firstpos+1, $length);
	endif;
	$video='';
	$video='<object width="640" height="505">';
	$video.='<param name="movie" value="';
	$video.='http://www.youtube.com/v/'.$code;
	$video.='&hl=en_US&fs=1&rel=0&color1=0x5d1719&color2=0xcd311b"></param>';
	$video.='<param name="allowFullScreen" value="true"></param>';
	$video.='<param name="allowscriptaccess" value="always"></param>';
	$video.='<embed src="';
	$video.='http://www.youtube.com/v/'.$code;
	$video.='&hl=en_US&fs=1&rel=0&color1=0x5d1719&color2=0xcd311b" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="640" height="505"></embed></object>';
	if($echo):
		echo $video;
	else:
		return $video;
	endif;
}

function get_photo_from_yt_link($link, $id, $echo=1)
{
	$start=strpos($link, '=');
	$end=strpos($link, '&');
	if($end):
		$code=substr($link, $start+1, ($end-$start)-1);
	else:
		$code=substr($link, $start+1, strlen($link));
	endif;

	$video='';
	$video='<a href="#" rel="'.$id.'" title="click to watch this video" class="video_image"><img src="http://img.youtube.com/vi/'.$code.'/0.jpg" width="200px" height="150px" style="border:none;" alt="video image"></a>';
	if($echo):
		echo $video;
	else:
		return $video;
	endif;
}
?>