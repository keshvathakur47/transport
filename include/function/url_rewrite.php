<?php

$conf_rewrite_url = array(
        /*
          'product'=>'products',
          'pdetail'=>'product',
          'content'=>'page',
          'news'=>'news',
          'home'=>'',
          'suppliers'=>'suppliers',
          'about-us'=>'about-us',
          'our_story'=>'our-story',
          'our_people'=>'our-people',
          'community'=>'community-involvement',
          'sales_team'=>'sales-team',
          'careers'=>'careers',
          'contact-us'=>'contact-us',
          'email_signup'=>'email-signup',
          'login'=>'login',
          'forgotpassword'=>'forgotpassword',
          'myaccount'=>'myaccount',
          'profile'=>'profile',
          'order_history'=>'order-history',
          'order_detail'=>'order-detail',
          'logout'=>'logout',
          'cart'=>'cart' */
);

function make_url($page, $query = NULL) {
    global $conf_rewrite_url;
    parse_str($query, $string);
    if (isset($conf_rewrite_url[strtolower($page)]))
        return _makeurl($page, $string);
    else
        return DIR_WS_SITE . '?page=' . $page . ($query ? '&' . $query : '');
}

function load_url() {
    global $conf_rewrite_url;
    $URL = $_SERVER['REQUEST_URI'];
    if (isset($url_array[$string_parts['0']])):
        _load($url_array[$string_parts['0']], $string_parts);
    else:
        if (count($string_parts) == 1) {
            if (substr($string_parts[0], 0, 1) != '?' AND substr($string_parts[0], 0, 5) != 'index') {
                $_REQUEST['page'] = $string_parts['0'];
            }
        }
    endif;
}

function _makeurl($page, $string) {

    switch ($page) {
        case 'product':
            if (isset($string['id'])):
                $object = get_object('category', $string['id']);
                if (is_object($object)):
                    if (isset($string['p'])) {
                        return DIR_WS_SITE . 'products/' . $object->urlname . '/' . $string['p'];
                    } else {
                        return DIR_WS_SITE . 'products/' . $object->urlname;
                    }
                else:
                    if (isset($string['p'])) {
                        return DIR_WS_SITE . 'products/' . $string['p'];
                    } else {
                        return DIR_WS_SITE . 'products';
                    }
                endif;
            else:
                if (isset($string['p'])) {
                    return DIR_WS_SITE . 'products/' . $string['p'];
                } else {
                    return DIR_WS_SITE . 'products';
                }
            endif;
            break;
        case 'pdetail':
            if (isset($string['id'])):
                $product = get_object('product', $string['id']);
                if (isset($string['parent_id'])):
                    $object = get_object('category', $string['parent_id']);
                    return DIR_WS_SITE . 'product/' . $object->urlname . '/' . $product->urlname;
                else:
                    return DIR_WS_SITE . 'product/' . $product->urlname;
                endif;
            else:
                return DIR_WS_SITE . 'products';
            endif;
            break;
        case 'content':
            if (isset($string['id'])):
                $object = get_object('content', $string['id']);
                return DIR_WS_SITE . 'page/' . $object->urlname;
            endif;
            break;
        case 'news':
            if (isset($string['p'])):
                return DIR_WS_SITE . 'news/' . $string['p'];
            else:
                return DIR_WS_SITE . 'news';
            endif;
            break;
        case 'news-detail':
            if (isset($string['id'])):
                $object = get_object('news', $string['id']);
                return DIR_WS_SITE . 'newsitem/' . $object->name;
            else:
                return DIR_WS_SITE . 'newsitem';
        endif;
        case 'home':
            return DIR_WS_SITE;
            break;
        case 'suppliers':
            return DIR_WS_SITE . 'suppliers';
            break;
        case 'about-us':
            return DIR_WS_SITE . 'about-us';
            break;
        case 'our_story':
            return DIR_WS_SITE . 'our-story';
            break;
        case 'our_people':
            return DIR_WS_SITE . 'our-people';
            break;
        case 'community':
            return DIR_WS_SITE . 'community-involvement';
            break;
        case 'sales_team':
            return DIR_WS_SITE . 'sales-team';
            break;
        case 'careers':
            return DIR_WS_SITE . 'careers';
            break;
        case 'contact-us':
            return DIR_WS_SITE . 'contact-us';
            break;
        case 'email_signup':
            return DIR_WS_SITE . 'email-signup';
            break;
        case 'login':
            if (isset($string['msg'])):
                return DIR_WS_SITE . 'login/' . $string['msg'];
            else:
                return DIR_WS_SITE . 'login';
            endif;
            break;
        case 'forgotpassword':
            if (isset($string['msg'])):
                return DIR_WS_SITE . 'forgotpassword/' . $string['msg'];
            else:
                return DIR_WS_SITE . 'forgotpassword';
            endif;
            break;
        case 'myaccount':
            return DIR_WS_SITE . 'myaccount';
            break;
        case 'profile':
            if (isset($string['msg'])):
                return DIR_WS_SITE . 'profile/' . $string['msg'];
            else:
                return DIR_WS_SITE . 'profile';
            endif;
            break;
        case 'order_history':
            return DIR_WS_SITE . 'order-history';
            break;
        case 'order_detail':
            if (isset($string['id'])):
                return DIR_WS_SITE . 'order-detail/' . $string['id'];
            else:
                return DIR_WS_SITE . 'order-detail';
        endif;
        case 'logout':
            return DIR_WS_SITE . 'logout';
            break;
        case 'cart':
            if (isset($string['msg'])):
                return DIR_WS_SITE . 'cart/' . $string['msg'];
            else:
                return DIR_WS_SITE . 'cart';
            endif;
            break;
        default: break;
    }
}

function _load($page, $string_parts) {
    global $conf_rewrite_url;

    switch ($page) {
        case 'home':
            return DIR_WS_SITE;
            break;
        case 'product':
            /* only category name is passed with the URL */
            if (count($string_parts) == 2):
                if (is_string($string_parts['1'])) {
                    $object = get_object_by_col('category', 'urlname', urldecode($string_parts['1']));
                    $_REQUEST['page'] = 'product';
                    $_GET['id'] = $object->id;
                } else {
                    $_REQUEST['page'] = 'product';
                    $_GET['p'] = $string_parts['1'];
                }
            /* no category name is passed - display all categories page */ elseif (count($string_parts) == 1):
                $_REQUEST['page'] = 'product';
                $_GET['id'] = 0;
            /* category name & page number is passed */
            elseif (count($string_parts) == 3):
                $object = get_object_by_col('category', 'urlname', urldecode($string_parts['1']));
                $_GET['p'] = $string_parts['2'];
                $_REQUEST['page'] = 'product';
                $_GET['id'] = $object->id;
            endif;
            break;
        case 'pdetail':
            if (count($string_parts) == 2):
                $object = get_object_by_col('product', 'urlname', urldecode($string_parts['1']));
                $_REQUEST['page'] = 'pdetail';
                $_GET['id'] = $object->id;
            elseif (count($string_parts) == 3):
                $category = get_object_by_col('category', 'urlname', urldecode($string_parts['1']));
                $object = get_object_by_col('product', 'urlname', urldecode($string_parts['2']));
                $_REQUEST['page'] = 'pdetail';
                $_GET['id'] = $object->id;
                $_GET['parent_id'] = $category->id;
            endif;
            break;
        case 'content':
            if (count($string_parts) == 2):
                $object = get_object_by_col('content', 'urlname', urldecode($string_parts['1']));
                $_REQUEST['page'] = 'content';
                $_GET['id'] = $object->id;
            endif;
            break;
        case 'news':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'news';
                $_GET['p'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'news';
            endif;
            break;
        case 'news-detail':
            if (count($string_parts) == 2):
                $object = get_object_by_col('news', 'name', urldecode($string_parts['1']));
                $_REQUEST['page'] = 'news-detail';
                $_GET['id'] = $object->id;
            else:
                $object = get_object_by_col('news', 'name', urldecode($string_parts['1']));
                $_REQUEST['page'] = 'news-detail';
                $_GET['id'] = $object->id;
                $_GET['p'] = $string_parts['2'];
            endif;
            break;
        case 'suppliers':
            $_REQUEST['page'] = 'suppliers';
            break;
        case 'about-us':
            $_REQUEST['page'] = 'about-us';
            break;
        case 'our_story':
            $_REQUEST['page'] = 'our_story';
            break;
        case 'our_people':
            $_REQUEST['page'] = 'our_people';
            break;
        case 'community':
            $_REQUEST['page'] = 'community';
            break;
        case 'sales_team':
            $_REQUEST['page'] = 'sales_team';
            break;
        case 'careers':
            $_REQUEST['page'] = 'careers';
            break;
        case 'contact-us':
            $_REQUEST['page'] = 'contact-us';
            break;
        case 'email_signup':
            $_REQUEST['page'] = 'email_signup';
            break;
        case 'login':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'login';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'login';
            endif;
            break;
        case 'forgotpassword':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'forgotpassword';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'forgotpassword';
            endif;
            break;
        case 'myaccount':
            $_REQUEST['page'] = 'myaccount';
            break;
        case 'profile':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'profile';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'profile';
            endif;
            break;
        case 'order_history':
            $_REQUEST['page'] = 'order_history';
            break;
        case 'order_detail':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'order_detail';
                $_GET['id'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'order_detail';
            endif;
            break;
        case 'logout':
            $_REQUEST['page'] = 'logout';
            break;
        case 'cart':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'cart';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'cart';
            endif;
            break;
        default:
    }
}

?>