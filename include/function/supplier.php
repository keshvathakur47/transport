<?php

function validate_user($table, $details = array(), $convert_md5 = 1) {
    if ($convert_md5 == '1'):
        $details['password'] = md5($details['password']);
    endif;
    $query = new query($table);
    $query->Where = "where username='$details[username]' and password='$details[password]'  AND user_type='user'";

    if ($user = $query->DisplayOne()):
        return $user;
    else:
        return false;
    endif;
}

function validate_admin($table, $details = array(), $convert_md5 = 1, $active = true) {
    if ($convert_md5 == '1'):
        $details['password'] = md5($details['password']);
    endif;

    $query = new query($table);
    $query->Where = "where username='$details[username]' and password='$details[password]'";
    if ($active):
        $query->Where.=" and is_active=1";
    endif;
    if ($user = $query->DisplayOne()):
        return $user;
    else:
        return false;
    endif;
}

function sanitize_array_for_db($array = array()) {
    foreach ($array as $k => $v) {
        $array[$k] = mysql_real_escape_string($v);
    }
    return $array;
}

function update_last_access_admin($id, $status) {
    $q = new query('admin_user');
    $q->Data['last_access'] = date("Y-m-d h:i:s");
    $q->Data['is_loggedin'] = $status;
    $q->Data['id'] = $id;
    $q->Update();
}

function cancelDeactivate($id) {
    $q = new query('user');
    $q->Data['deactivate'] = '0';
    $q->Data['id'] = $id;
    $q->Update();
}

function update_last_access($id, $status) {
    $q = new query('user');
    $q->Data['last_access'] = date("Y-m-d h:i:s");
    $q->Data['is_loggedin'] = $status;
    $q->Data['id'] = $id;
    $q->Update();
}

function download_users() {
    $users = new query('user');
    $users->DisplayAll();
    $users_arr = array();
    if ($users->GetNumRows()):
        while ($user = $users->GetArrayFromRecord()):
            $user['total orders'] = get_total_orders_by_user($user['id']);
            array_push($users_arr, $user);
        endwhile;
    endif;
    $file = make_csv_from_array($users_arr);
    $filename = "users" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function download_staff() {
    $users = new query('staff');
    $users->DisplayAll();
    $users_arr = array();
    if ($users->GetNumRows()):
        while ($user = $users->GetArrayFromRecord()):
            $user['total orders'] = get_total_orders_by_user($user['id']);
            array_push($users_arr, $user);
        endwhile;
    endif;
    $file = make_csv_from_array($users_arr);
    $filename = "Staff" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function download_search_users($email, $first_name, $last_name, $city, $country) {
    $search_users = new query('user');
    $search_users->Where = "where username='$email' OR firstname='$first_name' OR lastname='$last_name' OR city='$city' OR country='$country'";
    $search_users->DisplayAll();
    $search_users_arr = array();
    if ($search_users->GetNumRows()):
        while ($search_user = $search_users->GetArrayFromRecord()):
            $search_user['total orders'] = get_total_orders_by_user($search_user['id']);
            array_push($search_users_arr, $search_user);
        endwhile;
    endif;
    $file = make_csv_from_array($search_users_arr);
    $filename = "search_users" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function get_feed_product($k = null, $by_sku = FALSE) {
    global $account;
    if (is_object($account)) {
        $filename = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/data_all';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            $data = unserialize(fread($handle, filesize($filename)));
            $feed_products = $data[$k === TRUE ? 'categories' : 'products'];
            fclose($handle);
            if ($by_sku === TRUE) {
                foreach ($feed_products as $feed_product) {
                    if ($feed_product['sku'] == $k) {
                        return $feed_product;
                    }
                }
                return FALSE;
            }
            if (is_numeric($k)) {
                return isset($feed_products[$k]) ? $feed_products[$k] : FALSE;
            }
            return $feed_products;
        }
    }
    return FALSE;
}

function get_feed_category_structure($k = null, $GetSubCategories = FALSE, $force_original = FALSE) {
    global $account;
    if (is_object($account) && isset($_SESSION['supplier_category_structure_file'])) {
        $filename = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/' . $_SESSION['supplier_category_structure_file'];
        if (!file_exists($filename) || $force_original === TRUE) {
            $filename = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/category';
        }
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            $data = unserialize(fread($handle, filesize($filename)));
            fclose($handle);
            if ($k === TRUE) {
                foreach ($data as $k => $v) {
                    if ($v['parent_id']) {
                        unset($data[$k]);
                    }
                }
                return $data;
            }
            if (is_numeric($k)) {
                foreach ($data as $cat) {
                    if ($cat['id'] == $k) {
                        if ($GetSubCategories === TRUE) {
                            $cat['sub_cats'] = get_feed_sub_cats($k);
                        }
                        return $cat;
                    }
                }
                return FALSE;
            }
            return $data;
        }
    }
    return FALSE;
}

function get_feed_category_structure_by_date($date = null, $k = null, $GetSubCategories = FALSE) {
    if (!is_string($date)) {
        $GetSubCategories = $k;
        $k = $date;
        $date = '';
    }
    if (!$date) {
        $date = date('YmdHi');
    }
    global $account;
    if (is_object($account)) {
        $filename = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/category_' . $date;
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            $data = unserialize(fread($handle, filesize($filename)));
            fclose($handle);
            if ($k === TRUE) {
                foreach ($data as $k => $v) {
                    if ($v['parent_id']) {
                        unset($data[$k]);
                    }
                }
                return $data;
            }
            if (is_numeric($k)) {
                foreach ($data as $cat) {
                    if ($cat['id'] == $k) {
                        if ($GetSubCategories === TRUE) {
                            $cat['sub_cats'] = get_feed_sub_cats($k);
                        }
                        return $cat;
                    }
                }
                return FALSE;
            }
            return $data;
        }
    }
    return FALSE;
}

function get_feed_sub_cats($id) {
    $sub_cats = array();
    $feed_cats = get_feed_category_structure();
//    pr($feed_cats);die;
    foreach ($feed_cats as $cat) {
        if ($cat['parent_id'] == $id) {
            $sub_cats[] = $cat;
        }
    }
    return $sub_cats;
}

function get_structures($only_draft = FALSE) {
    global $account;
    if (is_object($account) && is_dir(DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/')) {
        $structures = scandir(DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/');
        foreach ($structures as $k => $v) {
            $suffix = '';
            if ($only_draft === TRUE) {
                $suffix = '_d_';
            }
            if (strpos($v, 'category' . $suffix) === FALSE || $v == 'category') {
                unset($structures[$k]);
            }
        }
        return $structures;
    }
    return FALSE;
}

function remove_draft_structure() {
    global $account;
    //get all draft structures
    if (is_object($account) && $structures = get_structures(TRUE)) {
        if (!empty($structures)) {
            $root = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/';
            foreach ($structures as $structure) {
                unlink($root . $structure);
            }
        }
    }
}

function make_structures_file($filepath) {
    $data = get_main_file_data();
    $handle = fopen($filepath, 'w+');
    fwrite($handle, $data);
    fclose($handle);
    chmod($filepath, 0777);
}

function update_structures_file($data = '', $serialized = FALSE) {
    global $account;
    $root_folder = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/';
    $filename = $_SESSION['supplier_category_structure_file'];
    $filepath = $root_folder . $filename;
    if (!$data) {
        $data = get_main_file_data();
        $serialized = TRUE;
    }
    if (!file_exists($filepath)) {
        make_structures_file($filepath);
    }
    if ($serialized !== TRUE) {
        $data = serialize($data);
    }
    $handle = fopen($filepath, 'w+');
    fwrite($handle, $data);
    fclose($handle);
}

function get_main_file_data($unserialized = FALSE) {
    global $account;
    $main_file_name = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/category';
    $data = '';
    if (file_exists($main_file_name)) {
        $handle = fopen($main_file_name, 'r');
        $data = fread($handle, filesize($main_file_name));
        fclose($handle);
    }
    return $unserialized === TRUE ? unserialize($data) : $data;
}

function get_structures_file_data($unserialized = FALSE) {
    global $account;
    $root_folder = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/';
    $filename = $_SESSION['supplier_category_structure_file'];
    $filepath = $root_folder . $filename;
    if (!file_exists($filepath)) {
        make_structures_file($filepath);
    }
//    unlink($filepath);die;
    $handle = fopen($filepath, 'r');
    $data = fread($handle, filesize($filepath));
    fclose($handle);
    return $unserialized === TRUE ? unserialize($data) : $data;
}

function delete_category_from_structure($category_id) {
    $data = get_feed_category_structure($category_id, TRUE);
    if (!empty($data['sub_cats'])) {
        foreach ($data['sub_cats'] as $sub_cat) {
            pr($sub_cat);
            $cat_id = $sub_cat['id'];
            delete_category_from_structure($cat_id);
        }
    }
    $data = get_structures_file_data(TRUE);
    if (!empty($data)) {
        foreach ($data as $k => $v) {
            if ($v['id'] == $category_id) {
                unset($data[$k]);
            }
        }
        update_structures_file($data);
    }
}

// All data from supplier setting table
function getSupplierSettingData($active = false) {
    global $account;
    $supplier_id = 0;
    if (is_object($account)) {
        $supplier_id = $account->id;
    }
    $QueryObj = new query('supplier_setting');
    $QueryObj->Where = " where supplier_id=$supplier_id";
    if ($active):
        $QueryObj->Where.= " and status=1";
    endif;
    $QueryObj->Where.= " group by name";
    return $record = $QueryObj->ListOfAllRecords('object');
}

// All data from supplier setting table
function updateSupplierSettingData($POST) {
    foreach ($POST['key'] as $k => $v):
        $QueryObj = new query('supplier_setting');
        $QueryObj->Data['id'] = $k;
        $QueryObj->Data['value'] = $v;
        //$QueryObj->print = 1;
        $QueryObj->Update();
    endforeach;
    return true;
}

function getSupplierSettingsByName($name, $active = false) {
    global $account;
    $supplier_id = 0;
    if (is_object($account)) {
        $supplier_id = $account->id;
    }

    $QueryObj = new query('supplier_setting');

    $QueryObj->Where = "where name='$name' and supplier_id='$supplier_id'";

    if ($active):

        $QueryObj->Where.= " and status = 1";

    endif;

    $QueryObj->DisplayAll();

    $ObjArray = array();

    if ($QueryObj->GetNumRows()):

        while ($object = $QueryObj->GetArrayFromRecord()):

            $ObjArray[] = $object;

        endwhile;

    endif;

    return $ObjArray;
}

function get_supplier_setting_control($key, $type, $value) {
    switch ($type) {
        case 'text':
            echo '<input type="text" name="key[' . $key . ']" value="' . $value . '" class="form-control" style="border-radius: 3px !important" />';
            break;
        case 'number':
            echo '<input type="number" name="key[' . $key . ']" value="' . $value . '" class="form-control" style="border-radius: 3px !important" step="any" />';
            break;
        case 'select':
            echo get_y_n_drop_down('key[' . $key . ']', $value);
            break;
        default: echo get_y_n_drop_down('key[' . $key . ']', $value);
    }
}

function get_feed_manufacturers() {
    $all_products = get_feed_product();
    $manufacturers = [];
    foreach ($all_products as $product) {
        if (trim($product['manufacturer']) && !in_array(trim($product['manufacturer']), $manufacturers)) {
            $manufacturers[] = trim($product['manufacturer']);
        }
    }
    return $manufacturers;
}

function get_feed_brands() {
    $all_products = get_feed_product();
    $brands = [];
    foreach ($all_products as $product) {
        if (trim($product['brand']) && !in_array(trim($product['brand']), $brands)) {
            $brands[] = trim($product['brand']);
        }
    }
    return $brands;
}

function get_feed_manufacturer_products($manufacturer) {
    $all_products = get_feed_product();
    $products = [];
    foreach ($all_products as $product) {
        if (trim($product['manufacturer']) == $manufacturer) {
            $products[] = $product;
        }
    }
    return $products;
}

function get_feed_manufacturer_brands($manufacturer) {
    if (is_array($manufacturer)) {
        $brands = array();
        if (!empty($manufacturer)) {
            foreach ($manufacturer as $manu) {
                $brands = array_merge($brands, get_feed_manufacturer_brands($manu));
            }
        }
        return $brands;
    }
    $all_products = get_feed_product();
    $brands = [];
    foreach ($all_products as $product) {
        if (trim($product['brand']) && trim($product['manufacturer']) == $manufacturer && !in_array(trim($product['brand']), $brands)) {
            $brands[] = trim($product['brand']);
        }
    }
    return $brands;
}

function get_feed_brand_products($brand) {
    $all_products = get_feed_product();
    $products = [];
    foreach ($all_products as $product) {
        if (trim($product['brand']) == $brand) {
            $products[] = $product;
        }
    }
    return $products;
}

function get_feed_manufacturer_brand_products($manufacturer, $brand) {
    $all_products = get_feed_product();
    $products = [];
    foreach ($all_products as $product) {
        if (trim($product['manufacturer']) == $manufacturer && trim($product['brand']) == $brand) {
            $products[] = $product;
        }
    }
    return $products;
}

function filter_products($manufacturers = NULL, $brands = NULL, $skip_all = FALSE) {
    if (
            is_array($manufacturers) && !empty($manufacturers) &&
            (!is_array($brands) || (is_array($brands) && empty($brands)))
    ) {
        $products = array();
        foreach ($manufacturers as $manufacturer) {
            $manufacturer_products = get_feed_manufacturer_products($manufacturer);
            foreach ($manufacturer_products as $product) {
                if (!array_key_exists($product['sku'], $products)) {
                    $products[$product['sku']] = $product;
                }
            }
        }
        return $products;
    } elseif (is_array($brands) && !empty($brands)) {
        $products = array();
        foreach ($brands as $brand) {
            $brand_products = get_feed_brand_products($brand);
            foreach ($brand_products as $product) {
                if (!array_key_exists($product['sku'], $products)) {
                    $products[$product['sku']] = $product;
                }
            }
        }
        return $products;
    } elseif ($skip_all === FALSE) {
        $all_products = get_feed_product();
        return $all_products;
    }
}

function get_feed_product_images() {
    $args = func_get_args();
    foreach ($args as $arg) {
        if (is_bool($arg)) {
            $only_name = $arg;
        } else {
            if ($arg === 'for_live_feed') {
                $$arg = TRUE;
            } else {
                $sku = $arg;
            }
        }
    }
    global $account;
    if (is_object($account)) {
        $filename = DIR_FS_SITE . 'feeds/' . $account->folder_name . '/data/image_data';
        if (file_exists($filename)) {
            $handle = fopen($filename, 'r');
            $data = unserialize(fread($handle, filesize($filename)));
            fclose($handle);
            $obj = new supplier_product_images;
            if (isset($for_live_feed) && $for_live_feed === TRUE) {
                $supplier_images = $obj->getSupplierProductImages(TRUE);
            } else {
                $supplier_images = $obj->getSupplierProductImages();
            }
            foreach ($supplier_images as $_sku => $supplier_image) {
                if (array_key_exists($_sku, $data)) {
                    $data[$_sku] = array_merge($data[$_sku], $supplier_image);
                } else {
                    $data[$_sku] = $supplier_image;
                }
            }
            if (isset($only_name) && $only_name === TRUE) {
                make_image_names($data);
            }
            if (isset($sku)) {
                return isset($data[$sku]) ? $data[$sku] : FALSE;
            }
            return $data;
        }
    }
    return FALSE;
}

function make_image_names(&$data) {
    foreach ($data as $sku => $images) {
        foreach ($images as $i => $image) {
            $data[$sku][$i] = get_image_name($image);
        }
    }
}

function get_image_name($image) {
    $temp = explode('/', $image);
    $img_name = end($temp);
    $img_name = str_replace(' ', '-', $img_name);
    return $img_name;
}

function getActualCommission($website_id, $date = '') {
    if (!$date) {
        $date = date('Y-m-d H:i:s');
    }
    $obj = new supplier_store_commission;
    $website_commission = $obj->getWebsiteCommission($website_id, $date);
    if ($website_commission) {
        return $website_commission;
    }

    $obj = new supplier_default_commission;
    return $obj->getGlobalCommission($date);
}

function external_shipping_update() {
    include_once(DIR_FS_SITE . 'include/functionClass/userWebsiteClass.php');
    include_once(DIR_FS_SITE . 'include/functionClass/feedsClass.php');
    include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

    global $account;
    $feed_id = feeds::getFeedIdByFolder($account->folder_name);
    $obj = new user_website_feed();
    $totalWebsitesId = $obj->getAllWebsitesIdByFeedId($feed_id);
    if (!empty($totalWebsitesId)) {
        foreach ($totalWebsitesId as $websiteId) {
            $obj = new userWebsite();
            $website = $obj->getActiveWebsiteById($websiteId->user_website);
            echo @file_get_contents($website->domain_name . "/include/external_shipping_update.php");
        }
    }
}

function external_tax_update() {
    include_once(DIR_FS_SITE . 'include/functionClass/userWebsiteClass.php');
    include_once(DIR_FS_SITE . 'include/functionClass/feedsClass.php');
    include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

    global $account;
    $feed_id = feeds::getFeedIdByFolder($account->folder_name);
    $obj = new user_website_feed();
    $totalWebsitesId = $obj->getAllWebsitesIdByFeedId($feed_id);
    if (!empty($totalWebsitesId)) {
        foreach ($totalWebsitesId as $websiteId) {
            $obj = new userWebsite();
            $website = $obj->getActiveWebsiteById($websiteId->user_website);
            echo @file_get_contents($website->domain_name . "/include/external_tax_update.php");
        }
    }
}

?>