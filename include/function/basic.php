<?php
include('url_rewrite.php');

/* sanitize funtion */

function sanitize($value) {
    $prohibited_chars = array(" ", "/", "$", "&", "'", '%', '"', "@", "");
    foreach ($prohibited_chars as $k => $v):
        $value = str_replace($v, '-', $value);
    endforeach;
    return strtolower($value);
}

function sanitize_webiste_name($value) {
    $prohibited_chars = array(" ", "/", "$", "&", "'", '%', '"', "@", "", ".");
    foreach ($prohibited_chars as $k => $v):
        $value = str_replace($v, '-', $value);
    endforeach;
    return strtolower($value);
}

function include_functions($functions) {
    foreach ($functions as $value):
        if (file_exists(DIR_FS_SITE . 'include/function/' . $value . '.php')):
            include_once(DIR_FS_SITE . 'include/function/' . $value . '.php');
        endif;
    endforeach;
}

function display_message($unset = 0) {
    $admin_user = new admin_session();
    if ($admin_user->isset_pass_msg()):
        if (isset($_SESSION[ADMIN_SESSION_NAME]['msg_type']) && $_SESSION[ADMIN_SESSION_NAME]['msg_type'] == 0):
            $error_type = 'danger';
            $icon_class = 'icon-warning-sign';
        else:
            $error_type = 'success';
            $icon_class = 'icon-ok';
        endif;
        foreach ($admin_user->get_pass_msg() as $value):
            echo '<div class="alert notyfy_message alert-' . $error_type . '">';

            echo '<button class="close" data-dismiss="alert">&times;</button>';
            echo $value;
            echo '</div>';

        endforeach;
    endif;
    ($unset) ? $admin_user->unset_pass_msg() : '';
}

function get_var_if_set($array, $var, $preset = '') {
    return isset($array[$var]) ? $array[$var] : $preset;
}

function get_var_set($array, $var, $var1) {
    if (isset($array[$var])):
        return $array[$var];
    else:
        return $var1;
    endif;
}

function get_template($template_name, $array, $selected = '') {
    include_once(DIR_FS_SITE . 'template/' . TEMPLATE . '/' . $template_name . '.php');
}

function get_meta($page) {
    $page = trim($page);
    if ($page != ''):
        $query = new query('keywords');
        $query->Where = "where page_name='$page'";
        if ($content = $query->DisplayOne()):
            return $content;
        else:
            return null;
        endif;
    endif;
    return null;
}

function head($content = '') {
    global $page;
    global $content;
    echo '<meta charset="utf-8"/>' . "\n";
    if (is_object($content)):
        ?>
        <title><?php echo isset($content->name) && $content->name ? $content->name : ''; ?></title>	
        <meta name="keywords" content="<?php echo isset($content->meta_keyword) ? $content->meta_keyword : ''; ?>" />
        <meta name="description" content="<?php echo isset($content->meta_description) ? $content->meta_description : ''; ?>" />
        <?php
    else:
        $content = get_meta($page);
        ?>
        <title><?php echo isset($content->page_title) && $content->page_title ? $content->page_title : ''; ?></title>
        <meta name="keywords" content="<?php echo isset($content->keyword) ? $content->keyword : ''; ?>" />
        <meta name="description" content="<?php echo isset($content->description) ? $content->description : ''; ?>" />
    <?php
    endif;
    echo '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>' . "\n";
    echo '<meta name="author" content="cWebconsultants"/>' . "\n";
    echo '<meta name="robots" content="index, follow" />' . "\n";
    echo '<meta name="software" content="cWebCart - Custom eCommerce Package" />' . "\n";
    echo '<link rel="shortcut icon" href="' . DIR_WS_SITE_GRAPHIC . 'icon_5.ico" />' . "\n";
//    include_once(DIR_FS_SITE . 'include/template/stats/google_analytics.php');
    ?>
    <?php
}

/* SEO Information......... */

function add_metatags($title = "", $keyword = "", $description = "") {
    /* description rule */
    $description_length = 150; /* characters */
    if (strlen($description) > $description_length)
        $description = substr($description, 0, $description_length);

    /* title rule */
    $title_length = 100;
    if (strlen($title) > $title_length)
        $title = SITE_NAME . ' | ' . substr($title, 0, $title_length);
    else
        $title = SITE_NAME . ' | ' . $title;
    $content = new stdClass();
    $content->name = ucwords($title);
    $content->meta_keyword = $keyword;
    $content->meta_description = ucfirst($description);
    return $content;
}

/*
  function css($array=array('reset','master')){
  echo "\n".'<link href="'.DIR_WS_SITE.'css/print.css" rel="stylesheet" type="text/css" media="print" >'."\n";
  echo '<link href="'.DIR_WS_SITE.'css/base.css" rel="stylesheet" type="text/css" media="screen" >'."\n";
  foreach ($array as $k=>$v):
  if($v=='style' && isset($_SESSION['use_stylesheet'])):
  echo '<link href="'.DIR_WS_SITE.'css/'.$_SESSION['use_stylesheet'].'.css" rel="stylesheet" type="text/css" media="screen, projection" >'."\n";
  else:
  echo '<link href="'.DIR_WS_SITE.'css/'.$v.'.css" rel="stylesheet" type="text/css" media="screen, projection" >'."\n";
  endif;
  endforeach;
  echo '<link href="'.DIR_WS_SITE.'css/menu/drop.css" rel="stylesheet" type="text/css" media="screen" >'."\n";

  }

  function js($array=array()){
  ?>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
  <?php
  echo '<script src="'.DIR_WS_SITE_JAVASCRIPT.'google_font.js" type="text/javascript"></script> '."\n";
  foreach ($array as $k=>$v):
  echo '<script src="'.DIR_WS_SITE.'javascript/'.$v.'.js" type="text/javascript"></script> '."\n";
  endforeach;
  }

  function body()
  {
  /*include all the body related things like... tracking code here. *//*

  }

  function footer()
  {
  /* if you need to add something to the website footer... please add here. *//*
  }
 */

function array_map_recursive($callback, $array) {
    $b = Array();
    foreach ($array as $key => $value) {
        $b[$key] = is_array($value) ? array_map_recursive($callback, $value) : call_user_func($callback, $value);
    }
    return $b;
}

function if_set_in_post_then_display($var) {
    if (isset($_POST[$var])):
        echo $_POST[$var];
    endif;
    echo '';
}

function validate_captcha() {
    global $privatekey;

    if ($_POST["recaptcha_response_field"]) {
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        if ($resp->is_valid) {
            return true;
        } else {
            /* set the error code so that we can display it */
            return false;
        }
    }
}

function is_set($array = array(), $item, $default = 1) {
    if (isset($array[$item]) && $array[$item] != 0) {
        return $array[$item];
    } else {
        return $default;
    }
}

function limit_text($text, $limit = 100) {
    if (strlen($text) > $limit):
        return substr($text, 0, strpos($text, ' ', $limit));
    else:
        return $text;
    endif;
}

function get_object($tablename, $id, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "where id='$id'";
    return $query->DisplayOne($type);
}

function get_object_by_col($tablename, $col, $col_value, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "where $col='$col_value'";
    return $query->DisplayOne($type);
}

function get_object_var($tablename, $id, $var) {
    $q = new query($tablename);
    $q->Field = "$var";
    $q->Where = "where id='" . $id . "'";
    if ($obj = $q->DisplayOne()):
        return $obj->$var;
    else:
        return false;
    endif;
}

function echo_y_or_n($status) {
    echo ($status) ? 'Yes' : 'No';
}

function target_dropdown($name, $selected = '', $tabindex = 1) {
    $values = array('new window' => '_blank', 'same window' => '_parent');
    echo '<select name="' . $name . '" size="1" tabindex="' . $tabindex . '">';
    foreach ($values as $k => $v):
        if ($v == $selected):
            echo '<option value="' . $v . '" selected>' . ucfirst($k) . '</option>';
        else:
            echo '<option value="' . $v . '">' . ucfirst($k) . '</option>';
        endif;
    endforeach;
    echo '</select>';
}

function make_csv_from_array($array) {
    $sr = 1;
    $heading = '';
    $file = '';
    foreach ($array as $k => $v):
        foreach ($v as $key => $value):
            if ($sr == 1):$heading.=$key . ', ';
            endif;
            $file.=str_replace("\r\n", "<<>>", str_replace(",", ".", html_entity_decode($value))) . ', ';
        endforeach;
        $file = substr($file, 0, strlen($file) - 2);
        $file.="\n";
        $sr++;
    endforeach;
    return $file = $heading . "\n" . $file;
}

function get_y_n_drop_down($name, $selected) {
    echo '<select class="form-control" name="' . $name . '" size="1">';
    if ($selected):
        echo '<option value="1" selected>Yes</option>';
        echo '<option value="0">No</option>';
    else:
        echo '<option value="0" selected>No</option>';
        echo '<option value="1">Yes</option>';
    endif;
    echo '</select>';
}

function get_setting_control($key, $type, $value) {
    switch ($type) {
        case 'text':
            echo '<input type="text" name="key[' . $key . ']" value="' . $value . '" size="30">';
            break;
        case 'select':
            echo get_y_n_drop_down('key[' . $key . ']', $value);
            break;
        default: echo get_y_n_drop_down('key[' . $key . ']', $value);
    }
}

function css_active($page, $value, $class) {
    if ($page == $value)
        echo 'class=' . $class;
}

function parse_into_array($string) {
    return explode(',', $string);
}

function MakeDataArray($post, $not) {
    $data = array();
    foreach ($post as $key => $value):
        if (!in_array($key, $not)):
            $data[$key] = $value;
        endif;
    endforeach;
    return $data;
}

function is_var_set_in_post($var, $check_value = false) {
    if (isset($_POST[$var])):
        if ($check_value):
            if ($_POST[$var] === $check_value):
                return true;
            else:
                return false;
            endif;
        endif;
        return true;
    else:
        return false;
    endif;
}

function display_form_error() {
    $login_session = new user_session();
    if ($login_session->isset_pass_msg()):
        $array = $login_session->get_pass_msg();
        ?>
        <div class="errorMsg">
            <?php
            foreach ($array as $value):
                echo html_entity_decode($value) . '<br/>';
            endforeach;
            ?>
        </div>
    <?php elseif (isset($_GET['msg']) && $_GET['msg'] != ''):
        ?>
        <div class="errorMsg">
            <?php echo html_entity_decode($_GET['msg']) . '<br/>'; ?>
        </div>
        <?php
    endif;
    $login_session->isset_pass_msg() ? $login_session->unset_pass_msg() : '';
}

function Redirect($URL) {
    header("location:$URL");
    exit;
}

function Redirect1($filename) {
    if (!headers_sent())
        header('Location: ' . $filename);
    else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . $filename . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';
        echo '</noscript>';
    }
}

function re_direct($URL) {
    header("location:$URL");
    exit;
}

function display_url($title, $page, $query = '', $class = '') {
    return '<a href="' . make_url($page, $query) . '" class="' . $class . '">' . $title . '</a>';
}

function make_admin_url($page, $action = 'list', $section = 'list', $query = '') {
    return DIR_WS_SITE_CONTROL . 'control.php?Page=' . $page . '&action=' . $action . '&section=' . $section . ($query ? '&' . $query : '');
}

function make_admin_url2($page, $action = 'list', $section = 'list', $query = '') {
    if ($page == 'home'):
        return DIR_WS_SITE . 'index.php';
    else:
        return DIR_WS_SITE_CONTROL . 'control.php?Page=' . $page . '&action2=' . $action . '&section2=' . $section . '&' . $query;
    endif;
}

function make_admin_link($url, $text, $title = '', $class = '', $alt = '') {
    return '<a href="' . $url . '" class="' . $class . '" title="' . $title . '" alt="' . $alt . '" >' . $text . '</a>';
}

function download_newsletterlist() {
    $users = new query('newsletterlist');
    $users->Field = "id,user_id,name,email,newsletter,promotion,ads";
    //$users->print=1;
    $users->DisplayAll();
    $users_arr = array();
    if ($users->GetNumRows()):
        while ($user = $users->GetArrayFromRecord()):

            array_push($users_arr, $user);
        endwhile;
    endif;
    $file = make_csv_from_array($users_arr);
    $filename = "newsletterlist" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function quit($message = 'processing stopped here') {
    echo $message;
    exit;
}

function download_orders($payment_status, $order_status) {
    $orders = new query('orders');
    $orders->Field = "id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";
    if ($order_status == 'paid'):
        $orders->Where = "where payment_status=" . $payment_status . " and order_status!='delivered'";
    elseif ($order_status == 'attempted'):
        $orders->Where = "where payment_status=" . $payment_status . " and order_status='received'";
    else:
        $orders->Where = "where payment_status=" . $payment_status . " and order_status='delivered'";
    endif;
    $orders->DisplayAll();
    #print_r($orders);exit();
    $orders_arr = array();
    if ($orders->GetNumRows()):
        while ($order = $orders->GetArrayFromRecord()):
            $order['Username'] = get_username_by_orders($order['user_id']);
            array_push($orders_arr, $order);
        endwhile;
    endif;
    $file = make_csv_from_array($orders_arr);
    $filename = "orders" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function get_username_by_orders($id) {
    if ($id == 0):
        return 'Guest';
    elseif ($id):
        $q = new query('user');
        $q->Field = "firstname,lastname";
        $q->Where = "where id='" . $id . "'";
        $o = $q->DisplayOne();
        return $o->firstname;
    endif;
}

function get_zones_box($selected = 0) {
    $q = new query('zone');
    $q->DisplayAll();
    echo '<select name="zone" size="1">';
    while ($obj = $q->GetObjectFromRecord()):
        if ($selected = $obj->id):
            echo '<option value="' . $obj->id . '" selected>' . $obj->name . '</option>';
        else:
            echo '<option value="' . $obj->id . '">' . $obj->name . '</option>';
        endif;
    endwhile;
    echo '</select>';
}

function zone_drop_down($zone_id, $id, $s, $z) {
    $query = new query('zone_country');
    $query->Where = "where zone_id=$zone_id";
    $query->DisplayAll();
    $country_list = array();
    $country_name = '';
    while ($object = $query->GetObjectFromRecord()):
        $country_name = get_country_name_by_id($object->country_id);
        $idd = $object->country_id;
        //$country_list('id'=>$object->id, 'name'=>$country_name));
        array_push($country_list, array('name' => $country_name, 'id' => $object->id));
    //$country_list[$object->id]=$country_name;
    endwhile;
    $total_list = array();
    foreach ($country_list as $k => $v):
        $total_list[] = $v['name'];
    endforeach;
    array_multisort($total_list, SORT_ASC, $country_list);
    /* print_r($country_list);exit; */
    echo '<select name="' . $id . '" size="10" style="width:200px;" multiple>';
    foreach ($country_list as $k => $v):
        if (($z == $zone_id) && $s):
            echo '<option value="' . $v['id'] . '" selected="selected">' . ucfirst($v['name']) . '</option>';
        else:
            echo '<option value="' . $v['id'] . '">' . ucfirst($v['name']) . '</option>';
        endif;
    endforeach;
    echo'</select>';
}

function get_supplier_dd($supplier) {
    $query = new query('supplier');
    $query->Where = "where is_active=1";
    $query->DisplayAll();
    $options = '<option value="">None</option>';
    while ($sup = $query->GetObjectFromRecord()) {
        if ($supplier && $supplier == $sup->name)
            $options.='<option value="' . $sup->name . '" selected>' . ucwords($sup->name) . '</option>';
        else
            $options.='<option value="' . $sup->name . '">' . ucwords($sup->name) . '</option>';
    }
    return $options;
}

function get_page_head_description($page = 'home') {
    $defoult_text_for_home = "Whether you live in the province, have visited Newfoundland, or are a homesick Newfie looking for some much-needed reminders of The Rock, Island Treasures is your one source for everything Newfoundland and Labrador.";
    $defoult_text = "Mauris hendrerit ligula ut sapien varius in adipiscing nisl cursus. Nullam varius lacinia hendrerit. Ut eleifend porttitor porta. In et justo justo, eget dignissim lorem. Etiam eleifend enim eu purus fermentum a vulputate diam dapibus.";
    switch ($page) {
        case 'product':
            global $category;
            if (isset($_GET['id'])):
                echo html_entity_decode(limit_text($category->description, 303));
            else:
                echo html_entity_decode($defoult_text);
            endif;
            break;
        case 'content':
            global $object;
            echo html_entity_decode(limit_text($object->top_content, 303));
            break;
        case 'home':
            global $object;
            echo html_entity_decode($defoult_text_for_home);
            break;
        case 'pdetail':
            global $object;
            echo html_entity_decode(limit_text($object->top_description, 303));
            break;
        default:
            echo html_entity_decode($defoult_text);
    }
}

/* get table name from module name */

function getModuleTable($module) {
    global $moduleTableForStatus;
    if (array_key_exists($module, $moduleTableForStatus)):
        return $moduleTableForStatus[$module];
    endif;
    return false;
}

;

# Create Website Function

function create_website($name, $website_id, $store_details = false, $store_name = false, $support_email = false, $store_business_name = false, $store_domain_name = false, $store_legal_name = false, $store_address1 = false, $store_address2 = false, $store_city = false, $store_state = false, $store_country = false, $store_zip = false, $store_phone = false) {
    global $is_local;
    global $is_subdomain;

    set_time_limit(0);

    $db_name = sanitize_webiste_name($name) . '-' . $website_id;

    $database_name = IMPORT_DB_PREFIX . $db_name;
    $createObj = new table();
    if ($createObj->createDatabase($database_name)):

        $createTableObj = new table();
        $createTableObj->createDatabaseTables($database_name);

        $createTableConstrnObj = new table();
        $createTableConstrnObj->createTableConstrain($database_name);

        $QueryObj = new defaultContent();
        $default_pages = $QueryObj->getAllDefaultPages();


        $addentryObj = new table();
        $addentryObj->addSampleData($database_name, $store_details->email, $store_name, $default_pages, $store_details->username, $store_details->password, $support_email, $store_business_name, $store_domain_name, $store_legal_name, $store_address1, $store_address2, $store_city, $store_state, $store_country, $store_zip, $store_phone);
    else:
        return false;
    endif;

    /* make webiste name here */
    $name = sanitize_webiste_name($name);
    if (is_dir(DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name)):
        $name = $name . '-' . $website_id;
    endif;

    ### Now Extract File 
    $file = IMPORT_SITE_ZIP_FILE . '.zip';
    if (!is_dir(DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name)):
        @mkdir(DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name, 0755);
        chmod(DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name, 0755);  // octal; correct value of mode
        copy(DIR_FS_SITE . IMPORT_FORLDER . '/' . $file, DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name . '/' . $file);

        $zip = new ZipArchive;
        $res = $zip->open(DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name . '/' . $file);

        if ($res === TRUE) {
            // extract it to the path we determined above

            $zip->extractTo(DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name);
            $zip->close();
            unlink(DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name . '/' . $file);

            #Now Change the connection file for this
            //read the entire string
            $str = implode("\n", file(DIR_FS_SITE . IMPORT_DESTINATION_FORLDER . '/' . $name . '/include/connection.php'));

            $fp = fopen(DIR_FS_SITE . '/' . IMPORT_DESTINATION_FORLDER . '/' . $name . '/include/connection.php', 'w');

            //replace variables in the file string 

            if ($is_local == 1) {

                $server_path = DIR_WS_SITE . IMPORT_DESTINATION_FORLDER;
                $server_path_ssl = DIR_WS_SITE_SSL . IMPORT_DESTINATION_FORLDER;
                $folder_path = "/" . $name . "/";
                $domain_path = $server_path . '/' . $name;
            } else {
                // subdomain path in case of server
                $server_path = make_website_url_with_username($name);
                $server_path_ssl = make_website_ssl_url_with_username($name);
                $folder_path = "/";
                $domain_path = $server_path;
            }


            $str = str_replace('SET_SERVER', $server_path, $str);
            $str = str_replace('SET_SSL_SERVER', $server_path_ssl, $str);
            $str = str_replace('FOLDER_NAME', $folder_path, $str);

            $str = str_replace('HOSTING', IMPORT_HOST, $str);
            $str = str_replace('DBNAME', $database_name, $str);
            $str = str_replace('DBUSER', IMPORT_USENAME, $str);
            $str = str_replace('DBPASS', IMPORT_PASSWORD, $str);

            fwrite($fp, $str, strlen($str));
            fclose($fp);

            $webiste_credentials = array();
            $webiste_credentials['website_folder'] = $name;
            $webiste_credentials['website_db_name'] = $database_name;
            $webiste_credentials['server_path'] = $domain_path;

            return $webiste_credentials;
        } else {
            return false;
        }
    endif;
    return false;
}

/* make website url with urlname */

function make_website_url_with_username($username = "") {
    $url = "";
    if ($username != "") {
        $input = HTTP_SERVER;

        // in case scheme relative URI is passed, e.g., //www.google.com/
        $input = trim($input, '/');

        // If scheme not included, prepend it
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }

        $urlParts = parse_url($input);

        // remove www
        $domain = preg_replace('/^www\./', '', $urlParts['host']);

        $url = "http://" . $username . "." . $domain;
    }

    return $url;
}

function make_website_ssl_url_with_username($username = "") {

    $url = "";

    if ($username != "") {
        $input = HTTP_SERVER_SSL;

        // in case scheme relative URI is passed, e.g., //www.google.com/
        $input = trim($input, '/');

        // If scheme not included, prepend it
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'https://' . $input;
        }

        $urlParts = parse_url($input);

        // remove www
        $domain = preg_replace('/^www\./', '', $urlParts['host']);

        $url = "https://" . $username . "." . $domain;
    }

    return $url;
}

# Get Website Visitor

function getWebsiteVisitor($host, $db, $user, $pass) {
    $total = '';
    ## Create db connection
    $con = mysql_connect($host, $user, $pass) or die('not connect');
    $select_db = mysql_select_db($db);

    if ($select_db):
        #execute the SQL query and return records
        $result = mysql_query("SELECT count(*) as total FROM " . TABLE_PREFIX . "web_stat", $con);
        if (mysql_num_rows($result) > 0):
            #fetch tha data from the database 
            while ($row = mysql_fetch_array($result)) {
                $total = $row['total'];
            }
            return $total;
        endif;
    else:
        return $total;
    endif;
}

/* get all returning users */

function getWebsiteVisitorStat($host, $db, $user, $pass) {
    $cat = array();
    $od = '';
    ## Create db connection
    $con = mysql_connect($host, $user, $pass) or die('not connect');
    $select_db = mysql_select_db($db);

    if ($select_db):
        #execute the SQL query and return records
        $result = mysql_query("SELECT id, SUBSTRING(MONTHNAME(on_date), 1, 3) AS month_y, count(*) as total FROM " . TABLE_PREFIX . "web_stat where YEAR(on_date)='" . date('Y') . "'", $con);

        if (mysql_num_rows($result) > 0):
            #fetch tha data from the database 
            while ($row = mysql_fetch_array($result)) {
                $cat[$row['month_y']] = $row['total'];
            }
        endif;
    else:
        return false;
    endif;


    $month = array('Jan' => 0, 'Feb' => 0, 'Mar' => 0, 'Apr' => 0, 'May' => 0, 'Jun' => 0, 'Jul' => 0, 'Aug' => 0, 'Sep' => 0, 'Oct' => 0, 'Nov' => 0, 'Dec' => 0);

    $mth_order = '';
    foreach ($month as $mk => $mv):
        if (array_key_exists($mk, $cat)):
            $mth_order.="['" . $mk . "'," . $cat[$mk] . "],";
        else:
            $mth_order.="['" . $mk . "'," . $mv . "],";
        endif;
    endforeach;

    $mth_order = substr_replace(trim($mth_order), "", -1);

    return $mth_order;
}

# All data from setting table

function getSettingData($active = false) {
    $QueryObj = new query('setting');
    $QueryObj->Where = " ";
    if ($active):
        $QueryObj->Where.= " where status = 1";
    endif;
    $QueryObj->Where.= " group by name";
    return $record = $QueryObj->ListOfAllRecords('object');
}

# All data from setting table

function updateSettingData($POST) {
    foreach ($POST['key'] as $k => $v):
        $QueryObj = new query('setting');
        $QueryObj->Data['id'] = $k;
        $QueryObj->Data['value'] = $v;
        $QueryObj->Update();
    endforeach;
    return true;
}

function select_month($selected = '') {
    $month = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');

    for ($i = 1; $i < 13; $i++):
        if ($i == $selected):
            echo '<option value="' . $i . '" selected>' . $month[$i] . '</option>';
        else:
            echo '<option value="' . $i . '">' . $month[$i] . '</option>';
        endif;
    endfor;
}

function select_year($selected = '') {
    $end = date(Y) + 1;
    $start = '2011';
    while ($end >= $start):
        if ($end == $selected):
            echo '<option value="' . $end . '" selected>' . $end . '</option>';
        else:
            echo '<option value="' . $end . '">' . $end . '</option>';
        endif;
        $end--;
    endwhile;
}

function pr($e, $show_count = FALSE) {
    if ($show_count === TRUE && is_array($e) && count($e) > 1) {
        echo "Count : " . count($e) . '<hr />';
    }
    echo '<pre>';
    print_r($e);
    echo '</pre>';
}
function default_supplier_inserts($sup_id) {
    // Settings
    $QueryObj = new query('supplier_setting');
    $Query = "INSERT INTO `$QueryObj->TableName` (`supplier_id`, `key`, `value`, `name`, `title`, `type`, `position`, `status`) VALUES
( $sup_id, 'PRODUCT_FEED_URL', '', 'feed', 'Product Feed URL', 'text', 0, 1),
( $sup_id, 'PHOTO_FEED_URL', '', 'feed', 'Photo Feed URL', 'text', 0, 1),
( $sup_id, 'QUANTITY_FEED_URL', '', 'feed', 'Quantity Feed URL', 'text', 0, 1),
( $sup_id, 'IS_SHIPPING_FREE', '1', 'shipping', 'Is Shipping Free', 'select', 0, 1),
( $sup_id, 'COMMISSION_BILLING_CYCLE', '30', 'commission', 'Commission Billing Cycle', 'number', 0, 1);";
    $QueryObj->ExecuteQuery($Query);

    // ---------------------------------Navigations------------------------------- //
    $QueryObj = new query('supplier_navigations');
    $Query = "INSERT INTO `$QueryObj->TableName` (`supplier_id`, `name`, `position`, `description`, `is_active`, `is_deleted`, `date_add`, `date_upd`) VALUES
($sup_id, 'Top Navigation Menu', 'top', NULL, '1', 0, '2015-01-14 15:52:50', '2015-03-26 12:04:29'),
($sup_id, 'Mobile Navigation Menu', 'mobile', NULL, '1', 0, '2015-01-14 15:52:50', '2015-03-26 12:04:35');";
    $QueryObj->ExecuteQuery($Query);

    //----------------------------------Default Commission Setting-----------------------------//
    $QueryObj = new query('supplier_default_commission');
    $Query = "INSERT INTO `$QueryObj->TableName` (`supplier_id`, `commission`, `start_date`) VALUES
($sup_id, 10, NOW());";
    $QueryObj->ExecuteQuery($Query);

    //---------------------------------Supplier Shipping Methods-----------------------------//
    $QueryObj = new query('supplier_shipping');
    $Query = "INSERT INTO `$QueryObj->TableName` (`supplier_id`, `name`, `s_key`, `code`, `carrier`, `desc`, `logo`, `tracking_url`, `is_active`, `is_deleted`, `is_default`, `sequence`, `position`, `date_add`, `handling_charges`) VALUES
($sup_id, 'Flat Rate', 'SHIPPING_FLAT_ID', 'flat', 'DPD', 'Flat price can be apply on per order or per product.', 'flat', '#', '0', '0', '1', 1, 1, NOW(), 0),
($sup_id, 'Price Based', 'SHIPPING_PRICE_ID', 'price', 'ONDOT', 'Shipping will be apply based on the order price.', 'price', '#', '1', '0', '0', 2, 2, NOW(), 0),
($sup_id, 'Weight Based', 'SHIPPING_WEIGHT_ID', 'weight', 'UPC', 'Order weight will decide the shipping cost.', 'weight', '#', '0', '0', '0', 3, 3, NOW(), 0);";
    $QueryObj->ExecuteQuery($Query);
}

function createUserGroupCondition($user_group, $alias = '') {
    $output = $table_alias = '';
    if ($user_group == 'logged_in_user'):
        global $logged_user_group_id;
        $user_group = $logged_user_group_id;
    endif;
    if ($alias != ''):
        $alias.= $table_alias . '.';
    endif;
    if ($user_group != ''):
        $user_group_array = explode(',', trim($user_group));
        $user_group_array = array_unique($user_group_array);
        if (!in_array('0', $user_group_array)):
            $user_group_array[] = "0";
        endif;
        if (count($user_group_array) && $user_group_array[0] != ''):
            $total_user_groups = count($user_group_array);
            $sr = 1;
            $output.= ' and ';
            foreach ($user_group_array as $user_group_id):
                if ($total_user_groups > 1 && $sr == 1):
                    $output.=" ( ( ";
                elseif ($total_user_groups > 1):
                    $output.=" ( ";
                endif;

                $output.=" FIND_IN_SET (" . $user_group_id . ", " . $alias . "user_group_id) ";

                if ($total_user_groups > 1):
                    $output.=" ) ";
                endif;

                if ($total_user_groups > 1 && $sr != $total_user_groups):
                    $output.=" || ";
                elseif ($total_user_groups > 1):
                    $output.=" ) ";
                endif;

                $sr++;
            endforeach;
            $output.=" ";
        endif;
    endif;
    //echo $output;exit;
    return $output;
}

/* show price */

function show_price($price, $return = false) {
    $result = '';
    $price = number_format($price, 2);
    $result.=CURRENCY_SYMBOL;


    $result.=$price;


    if ($return):
        return $result;
    else:
        echo $result;
    endif;
}

/* get all active countries */

function getCountries($active = true) {
    $QueryObj = new query('supplier_country');
    if ($active):
        $QueryObj->Where = " where is_active='1'";
    else:
        $QueryObj->Where = " where 1=1";
    endif;
    $QueryObj->Where.=" order by short_name asc";
    return $QueryObj->ListOfAllRecords();
}
?>