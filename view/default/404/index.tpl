<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        {head}
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans:700,300italic,400italic,700italic,300,400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Russo+One' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        {add_css name='plugins/bootstrap/css/bootstrap.min,plugins/font-awesome/css/font-awesome,plugins/elegant_font/css/style,plugins/owl-carousel/owl.theme'}
        <link href="{DIR_WS_SITE_CSS}style.css" rel="stylesheet" />
        <link href="{DIR_WS_SITE_CSS}color-styles/styles.css" id="theme-style" rel="stylesheet" />
    </head>
    <body class="home-page">
        {include '../include/top.tpl'}
        {include 'content.tpl'}
        {include '../include/footer.tpl'}
    </body>
</html>