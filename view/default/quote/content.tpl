<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="margin-top: 40px">
            <h2 class="text-center" style="text-transform: uppercase">Auto Transport Quote</h2>
            <h1 class="text-center" style="color: #2586b7;">(111) 111-1111</h1>
            <p class="text-center" style="margin-bottom: 20px">Get the Vehile Shipping Quote Below</p>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-11 center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Get Shipping Quote</h3>
                    </div>
                    <div class="panel-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="">Tell us about the vehicle you want to ship.</label>
                                {assign var="start" value=2015}
                                <select name="year" id="year" class="form-control" autocomplete="off">
                                    <option value="">Year</option>
                                    {for $i=$start to 1900 step=-1}
                                        <option value="{$i}">{$i}</option>
                                    {/for}
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="make" id="make" class="form-control" autocomplete="off">
                                    <option value="">Make</option>
                                    <option value="Other">Other</option>
                                    <option value="Boat">Boat</option>
                                    <option value="RV">RV</option>
                                    <option value="Motorcycle">Motorcycle</option>
                                    <option value="AC">AC</option>
                                    <option value="Acura">Acura</option>
                                    <option value="Alfa Romeo">Alfa Romeo</option>
                                    <option value="AM General">AM General</option>
                                    <option value="AMC">AMC</option>
                                    <option value="Aston Martin">Aston Martin</option>
                                    <option value="Audi">Audi</option>
                                    <option value="Austin">Austin</option>
                                    <option value="Austin Healey">Austin Healey</option>
                                    <option value="Bentley">Bentley</option>
                                    <option value="BMW">BMW</option>
                                    <option value="Bricklin">Bricklin</option>
                                    <option value="Buick">Buick</option>
                                    <option value="Cadillac">Cadillac</option>
                                    <option value="Checker">Checker</option>
                                    <option value="Chevrolet">Chevrolet</option>
                                    <option value="Chrysler">Chrysler</option>
                                    <option value="Cord">Cord</option>
                                    <option value="Daewoo">Daewoo</option>
                                    <option value="Daihatsu">Daihatsu</option>
                                    <option value="Daimler">Daimler</option>
                                    <option value="Datsun">Datsun</option>
                                    <option value="DeLorean">DeLorean</option>
                                    <option value="DeSoto">DeSoto</option>
                                    <option value="Detomaso">Detomaso</option>
                                    <option value="Dodge">Dodge</option>
                                    <option value="Eagle">Eagle</option>
                                    <option value="Edsel">Edsel</option>
                                    <option value="Ferrari">Ferrari</option>
                                    <option value="Fiat">Fiat</option>
                                    <option value="Ford">Ford</option>
                                    <option value="Galloper">Galloper</option>
                                    <option value="Geo">Geo</option>
                                    <option value="GMC">GMC</option>
                                    <option value="Honda">Honda</option>
                                    <option value="Hummer">Hummer</option>
                                    <option value="Hyundai">Hyundai</option>
                                    <option value="Infiniti">Infiniti</option>
                                    <option value="International">International</option>
                                    <option value="Isuzu">Isuzu</option>
                                    <option value="Jaguar">Jaguar</option>
                                    <option value="Jeep">Jeep</option>
                                    <option value="Jensen">Jensen</option>
                                    <option value="Kia">Kia</option>
                                    <option value="Lamborghini">Lamborghini</option>
                                    <option value="Land Rover">Land Rover</option>
                                    <option value="Lexus">Lexus</option>
                                    <option value="Lincoln">Lincoln</option>
                                    <option value="Lotus">Lotus</option>
                                    <option value="Maserati">Maserati</option>
                                    <option value="Maybach">Maybach</option>
                                    <option value="Mazda">Mazda</option>
                                    <option value="Mercedes-Benz">Mercedes-Benz</option>
                                    <option value="Mercury">Mercury</option>
                                    <option value="Merkur">Merkur</option>
                                    <option value="MG">MG</option>
                                    <option value="Mini">Mini</option>
                                    <option value="Mitsubishi">Mitsubishi</option>
                                    <option value="Morris">Morris</option>
                                    <option value="Nash">Nash</option>
                                    <option value="Nissan">Nissan</option>
                                    <option value="Oldsmobile">Oldsmobile</option>
                                    <option value="Opel">Opel</option>
                                    <option value="Packard">Packard</option>
                                    <option value="Peugeot">Peugeot</option>
                                    <option value="Plymouth">Plymouth</option>
                                    <option value="Pontiac">Pontiac</option>
                                    <option value="Porsche">Porsche</option>
                                    <option value="Renault">Renault</option>
                                    <option value="Rolls-Royce">Rolls-Royce</option>
                                    <option value="Saab">Saab</option>
                                    <option value="Saturn">Saturn</option>
                                    <option value="Scion">Scion</option>
                                    <option value="Shelby">Shelby</option>
                                    <option value="Smart">Smart</option>
                                    <option value="Studebaker">Studebaker</option>
                                    <option value="Subaru">Subaru</option>
                                    <option value="Sunbeam">Sunbeam</option>
                                    <option value="Suzuki">Suzuki</option>
                                    <option value="Tata">Tata</option>
                                    <option value="Toyota">Toyota</option>
                                    <option value="Triumph">Triumph</option>
                                    <option value="Volkswagen">Volkswagen</option>
                                    <option value="Volvo">Volvo</option>
                                    <option value="Willys">Willys</option>
                                    <option value="Yugo">Yugo</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="model" id="model" class="form-control" autocomplete="off">
                                    <option value="">Model</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Next <i class="fa fa-chevron-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>