<div class="container" style="margin-top: 40px">
    <div class="row">
        <div class="col-md-6">
            <div class="col-xs-12">
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <center>Select your Shipping Type</center>
                        <hr style="margin: 2px 0 6px"/>
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#domestic_shipping_tab" data-toggle="tab">Domestic Shipping</a></li>
                            <li><a href="#international_shipping_tab" data-toggle="tab">International Shipping</a></li>
                        </ul>
                    </div>
                    <div class="panel-body" style="padding-top: 25px">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="domestic_shipping_tab">
                                <h3 style=";margin: 5px 0;padding-bottom: 10px;border-bottom: solid 1px #eee">Step 1:</h3>
                                <form method="post">
                                    <label>Source & Destination</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Source ZIP Code or City, State" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Destination ZIP Code or City, State" />
                                    </div>
                                    <label for="">Vehile Details:</label>
                                    <div class="form-group">
                                        {assign var="start" value=2015}
                                        <select name="year" id="year" class="form-control" autocomplete="off">
                                            <option value="">Year</option>
                                            {for $i=$start to 1900 step=-1}
                                                <option value="{$i}">{$i}</option>
                                            {/for}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="make" id="make" class="form-control" autocomplete="off">
                                            <option value="">Make</option>
                                            {foreach from=$makes item=make}
                                                <option value="{$make->id}">{$make->name}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="model" id="model" class="form-control" autocomplete="off">
                                            <option value="">Model</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Next <i class="fa fa-chevron-right"></i></button>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="international_shipping_tab">
                                <form method="post">
                                    <div class="form-group">
                                        <label for="">Tell us about the vehicle you want to ship.</label>
                                        {assign var="start" value=2015}
                                        <select name="year" id="year" class="form-control" autocomplete="off">
                                            <option value="">Year</option>
                                            {for $i=$start to 1900 step=-1}
                                                <option value="{$i}">{$i}</option>
                                            {/for}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="make" id="make" class="form-control" autocomplete="off">
                                            <option value="">Make</option>
                                            {foreach from=$makes item=make}
                                                <option value="{$make->id}">{$make->name}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="model" id="model" class="form-control" autocomplete="off">
                                            <option value="">Model</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Next <i class="fa fa-chevron-right"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="color: #fff">
            <h2 class="text-center" style="text-transform: uppercase">Auto Transport Quote</h2>
            <h1 class="text-center" style="color: #2586b7;">(111) 111-1111</h1>
            <p class="text-center" style="margin-bottom: 20px">Get the Vehile Shipping Quote Below</p>
        </div>
    </div>
</div>