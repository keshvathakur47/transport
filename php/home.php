<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
require_once DIR_FS_SITE . 'admin/include/functionClass/vehicleClass.php';

$content = add_metatags("Home", 'Home', 'Home');

$obj = new vehicle_manufacturer;
$makes = $obj->getManufacturer();

$smarty->assign_notnull('makes', $makes);
$smarty->renderLayout();
?>